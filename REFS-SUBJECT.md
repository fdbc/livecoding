## General references

#### Functional programming review

[_Why Functional Programming Matters_](http://www.cse.chalmers.se/~rjmh/Papers/whyfp.html). One of the best papers aiming at motivating functional programming.

[_Datatype-Generic programming_](http://www.cs.ox.ac.uk/jeremy.gibbons/publications/dgp.pdf). You will find in section 2 a very good account of modularity techniques. It is an ideal complement to the last paper.

[_An invitation to functional programming_](https://www.youtube.com/watch?v=aa7jh1J4xNs). An introductory talk on functional programming, by one of the authors of "Functional programming in Scala".

[_The Interpreter Pattern Revisited_](https://www.youtube.com/watch?v=hmX2s3pe_qk). Good talk showing the links between object-oriented patterns and functional programming.

[_Functional Programming is Terrible_](http://functionaltalks.org/2014/03/31/runar-oli-bjarnason-functional-programming-is-terrible/). Things that must be taken into account when using Scala for functional programming in production environments.

[_The algebra of algebraic data types_](http://chris-taylor.github.io/blog/2013/02/11/the-algebra-of-algebraic-data-types-part-ii/). Why algebraic data type are called algebraic?

[_Memoizing Polymorphic Functions with High School Algebra and Quantifiers_](http://blog.sigfpe.com/2009/11/memoizing-polymorphic-functions-with.html). Application of algebraic laws to the problem of memoizing polymorphic functions.

[_Why is ADT pattern matching allowed?_](http://typelevel.org/blog/2014/11/10/why_is_adt_pattern_matching_allowed.html). Discussion on pattern matching, parametricity and algebraic data types. It mentions in passing a trick to implement pattern matching without `match` expressions.

[_Fixing GADTs_](http://www.timphilipwilliams.com/posts/2013-01-16-fixing-gadts.html.). Folds for GADTs.

[_Polymorphic function values_](http://milessabin.com/blog/2012/04/27/shapeless-polymorphic-function-values-1/). How to implement generic functions as objects in Scala.

#### Type classes

[Typeclassopedia](https://wiki.haskell.org/Typeclassopedia). Classical reference in the introduction of the most common type classes: functors, monads, foldable, applicative, etc.

[The Neophyte's Guide to Scala Part 12: Type Classes](http://danielwestheide.com/blog/2013/02/06/the-neophytes-guide-to-scala-part-12-type-classes.html). A very good way of introducing type classes to object-oriented programmers.

[_Scala Typeclassopedia with John Kodumal_](youtube.com/watch?v=IMGCDph1fNY). The typeclassopedia (functors, monads, and so forth), translated into Scala.

[_Extensibility for the Masses. Practical Extensibility with Object Algebras_](https://www.cs.utexas.edu/~wcook/Drafts/2012/ecoop2012.pdf). Basic reference for the shallow embedding approach to DSL design. Object algebras are closely related to type classes. Examples are in Java! You can also find [here](http://i.cs.hku.hk/~bruno/oa/) a translation to other languages, including Scala.

#### Monoids

[_Haskell monoids and their uses_](http://blog.sigfpe.com/2009/01/haskell-monoids-and-their-uses.html). Or why monoid matters?

[_Monoids and finger trees_](http://apfelmus.nfshost.com/articles/monoid-fingertree.html). Nice applications of monoids to data structure design.

[_Monoids: Theme and Variations_](http://repository.upenn.edu/cis_papers/762/). Very good motivation for monoids, illustrated with the design of a library for graphics design.

#### Functors

[_Functors_](http://bartoszmilewski.com/2015/01/20/functors/). This post puts forward a generalised view of functors as containers.

[_The functor design pattern_](http://www.haskellforall.com/2012/09/the-functor-design-pattern.html). A more advanced post on functors, which don't simply focus on endofunctors, but on the more general perspective of category theory.

#### Scalacheck

[_ScalaCheck: The Definitive Guide_](http://www.artima.com/shop/scalacheck). The book on Scalacheck.

#### Scalaz

[_Learning scalaz_](http://eed3si9n.com/learning-scalaz/). A web-page documenting how to use scalaz, inspired in the "Learn you a Haskell ..." book.

[_Scalaz presentation_](http://functionaltalks.org/2013/05/27/nick-partridge-scalaz). Very nice introduction to the Scalaz library.

[_Functional Programming with Scalaz_](https://skillsmatter.com/skillscasts/2130-talk-by-jason-zaugg). Another great introduction to Scalaz, this time by one of its major contributors.

#### Cats

[_Advanced Scala with Cats_](http://underscore.io/books/advanced-scala/). Book published by Underscore that goes over a lot of functional programming abstractions contained in Cats library.

[_Machinist_](http://typelevel.org/blog/2013/10/13/spires-ops-macros.html). Blog post on Machinist, the library used in Cats to speed up the use of implicit operators on type classes.

[_Herding cats_](http://eed3si9n.com/herding-cats/). Blog by Eugene Yokota on cats.

## Monadic programming

[_The essence of functional programming_](http://www.eliza.ch/doc/wadler92essence_of_FP.pdf). Classical paper revolving around monads and their role within purely functional programming.

[_You Could Have Invented Monads! (And Maybe You Already Have.)_](http://blog.sigfpe.com/2006/08/you-could-have-invented-monads-and.html). Another classical blog post on monad.

[_Designing Fail-Fast Error Handling_](http://underscore.io/blog/posts/2015/02/23/designing-fail-fast-error-handling.html). Application of monads to the error handling problem.

#### Monad transformers

[_Monad Transformers Step by Step_](http://www.cs.virginia.edu/~wh5a/personal/Transformers.pdf). One of the most acclaimed introductions to monad transformers.

#### Free monads

[_Why free monads matter_](http://www.haskellforall.com/2012/06/you-could-have-invented-free-monads.html). Good introduction to free monads, in Haskell.

[_Free monads_](http://timperrett.com/2013/11/21/free-monads-part-1/). Good introduction to free monads, in Scala.

[_Purely Functional I/O_](http://www.infoq.com/presentations/io-functional-side-effects). Illustrating the free monad approach in the IO domain.

[_The operational monad tutorial_](http://apfelmus.nfshost.com/articles/operational-monad.html). An alternative presentation of Free monads to the one shown in class (basically, representing the free monad as a sequence, instead of a tree of effects).

#### Coproducts

[_Data types à la carte_](http://www.cs.ru.nl/~W.Swierstra/Publications/DataTypesALaCarte.pdf). How to combine effects using free monads.

[_Compositional Application Architecture With Reasonably Priced Monads_](http://functionaltalks.org/2014/11/23/runar-oli-bjarnason-free-monad/). Illustration of the approach put forward by the last paper in Scala.

## Applicative programming

[_Idioms: applicative programming with effects_](http://strictlypositive.org/Idiom.pdf). The paper that introduced the concept of applicative functor.

[_The Essence of the Iterator Pattern_](http://www.cs.ox.ac.uk/jeremy.gibbons/publications/iterator.pdf). Beautiful article on one of the most fruitful applications of applicative functors: traversals.

[_Free Applicative Functors_](http://www.paolocapriotti.com/assets/applicative.pdf). Free applicative definitions, and typical examples of static analysis.

## Generic programming

[_Datatype-Generic programming_](http://www.cs.ox.ac.uk/jeremy.gibbons/publications/dgp.pdf). General introduction to the topic of generic programming.

#### Shapeless

[_The Type Astronaut's Guide to Shapeless_](https://github.com/underscoreio/shapeless-guide). A great book on shapeless.

[_The swiss army knife of generic programming — shapeless's TypeClass type class in action_](https://docs.google.com/file/d/0B9fGApX0ZqFRcDBkYkg1STVxZEE/edit). ADTs, and their generic representation in shapeless in terms of HLists and Coproducts. Application to automatic type class derivation.

[_Introduction to Shapeless with applications from scodec_](https://speakerdeck.com/mpilquist/introduction-to-shapeless-with-applications-from-scodec). Good introduction to shapeless, and good application.

[_Solving problems in a generic way using Shapeless_](http://www.cakesolutions.net/teamblogs/solving-problems-in-a-generic-way-using-shapeless). Throughout example of automatic type class derivation using the type class TypeClass.