## Referencias

#### Books

[_Functional programming in Scala_](https://www.manning.com/books/functional-programming-in-scala). One of the best books for learning functional programming. As an added bonus, Scala is used throughout the book.

[_Learn you a Haskell for Great Good_](http://learnyouahaskell.com/). A book on functional programming in Haskell, well known for its clarity of exposition.

[_Scala Design Patterns_](http://link.springer.com/book/10.1007/978-3-319-02192-8). A translation to Scala of the popular object-oriented patterns, plus a section dedicated to functional programming patterns.

[_Advanced Scala with Scalaz_](http://underscore.io/books/advanced-scala-scalaz/). Small book on some of the main type classes included in the scalaz library.

[_Underscore books_](http://underscore.io/books/). A collection of books published by Underscore.

[_Haskell programming from first principles_](http://haskellbook.com/). Great book for learning Haskell from scratch.

#### Presentations

[_Functional talks_](http://functionaltalks.org). A curated collection of talks and presentations that exclusively focus on functional programming topics. Programming languages: Haskell, Scala, Clojure, etc.

[_An invitation to functional programming_](https://www.youtube.com/watch?v=aa7jh1J4xNs). An introductory talk on functional programming, by one of the authors of "Functional programming in Scala".

[_The Interpreter Pattern Revisited_](https://www.youtube.com/watch?v=hmX2s3pe_qk). Good talk showing the links between object-oriented patterns and functional programming.

[_Functional Programming is Terrible_](http://functionaltalks.org/2014/03/31/runar-oli-bjarnason-functional-programming-is-terrible/). Things that must be taken into account when using Scala for functional programming in production environments.

[_Scala Typeclassopedia with John Kodumal_](youtube.com/watch?v=IMGCDph1fNY). The typeclassopedia (functors, monads, and so forth), translated into Scala.

[_Scalaz presentation_](http://functionaltalks.org/2013/05/27/nick-partridge-scalaz). Very nice introduction to the Scalaz library.

[_Functional Programming with Scalaz_](https://skillsmatter.com/skillscasts/2130-talk-by-jason-zaugg). Another great introduction to Scalaz, this time by its major contributor.

[_Spoiled by higher-kinded types_](https://speakerdeck.com/adelbertc/spoiled-by-higher-kinded-types).

#### Blogs
[_Higher Order. Philosophy and functional programming_](http://blog.higher-order.com/). Advanced content on functional programming.

[_Bartosz Milewski's Programming Cafe. Concurrency, C++, Haskell, Category Theory_](http://bartoszmilewski.com/). Advanced content on functional programming and category theory. One of the best sources to learn category theory, BTW.

[_Purely functional_](http://blog.hablapps.com). Eventuall, our blog will be populated with many entries on functional programming ;)

[_Apfelmus_](http://apfelmus.nfshost.com/). Good tutorials on monoids and operational monads (something similar to the Free monad).

[_Haskell for all_](http://www.haskellforall.com/). One of the best sources to learn about functional programming, i.e. it's not only about haskell.

[_School of Haskell_](https://www.schoolofhaskell.com/). Again, this is about haskell, and henceforth about functional programming.

[_A Neighborhood of Infinity_](http://blog.sigfpe.com/). One of the best blogs on functional programming, using Haskell.

[_Typelevel blog_](http://typelevel.org/blog). Very good posts on Scala and functional programming.

[_Comonad reader_](http://comonad.com/reader/). VERY advanced blog posts by Edward K. Mett, amongst others.

#### Conferences

[_Lambda world_](http://lambda.world). The premier conference of functional programming for the software practitioner (at least in Spain ;). [Videos](https://www.youtube.com/channel/UCEBcDOjv-bhAmLavY71RMHA/feed).

[_Scala exchange_](http://scala.exchange). One of the most popular conferences for the Scala programming language. Based on UK.

[_Scaladays_](http://www.scaladays.org). The mother of all Scala conferences.

[_Scala world_](http://www.scala.world). A recent conference on Scala.

#### Papers

[_Why Functional Programming Matters_](http://www.cse.chalmers.se/~rjmh/Papers/whyfp.html). One of the best motivations for functional programming, even if monads were still in its infancy at the time of its writing.

[_How functional programming mattered_](https://doi.org/10.1093/nsr/nwv042). A sequel of the previous article written 26 years later.

[_The essence of functional programming_](http://www.eliza.ch/doc/wadler92essence_of_FP.pdf). This paper revolves around monads and its role within purely functional programming.

[_Folding Domain-Specific Languages: Deep and Shallow Embeddings_](https://pdfs.semanticscholar.org/20f7/202f1aaee983c9a39cd98384b88ce3d764ba.pdf). Essential paper to understand the relationships between datatype-based and typclass-based DSLs.

[_Freer Monads, More Extensible Effects_](http://okmij.org/ftp/Haskell/extensible/more.pdf). Paper that introduces the Eff monad, and its advantages over conventional Free monads (better composability, efficiency and interpreter modularity).

[_Datatype-Generic Programming_](http://www.cs.ox.ac.uk/jeremy.gibbons/publications/dgp.pdf).  General introduction to the topic of generic programming. Also, you will find in section 2 a very good account of modularity techniques.

[_Scala for Generic Programmers_](http://www.cs.ox.ac.uk/jeremy.gibbons/publications/scalagp-jfp.pdf). An in-depth comparison between Haskell and Scala from a generic programming perspective.

[_Data types à la carte_](http://www.cs.ru.nl/~W.Swierstra/Publications/DataTypesALaCarte.pdf). How to combine effects using free monads.

[_Idioms: applicative programming with effects_](http://strictlypositive.org/Idiom.pdf). The paper that introduced the concept of applicative functor.

[_The Essence of the Iterator Pattern_](http://www.cs.ox.ac.uk/jeremy.gibbons/publications/iterator.pdf). Beautiful article on one of the most fruitful applications of applicative functors: traversals.

[_Free Applicative Functors_](http://www.paolocapriotti.com/assets/applicative.pdf). Free applicative definitions, and typical examples of static analysis.

[_Monoids: Theme and Variations_](http://repository.upenn.edu/cis_papers/762/). Very good motivation for monoids, illustrated with the design of a library for graphics design.

[_Extensibility for the Masses. Practical Extensibility with Object Algebras_](https://www.cs.utexas.edu/~wcook/Drafts/2012/ecoop2012.pdf). Basic reference for the shallow embedding approach to DSL design. Object algebras are closely related to type classes. Examples are in Java! You can also find [here](http://i.cs.hku.hk/~bruno/oa/) a translation to other languages, including Scala.

[_The Fun of Programming_](https://www.cs.ox.ac.uk/publications/books/fop/). A great collection papers.

#### Other sources

[_Scala syntax_](http://docs.scala-lang.org/tutorials/scala-for-java-programmers.html). Summary of Scala for Java programmers.

[_Haskell wiki_](https://wiki.haskell.org/Haskell). The haskell wiki, a great source to steal functional programming concepts and apply them to Scala. For instance, you'll find there the [Typeclassopedia](https://wiki.haskell.org/Typeclassopedia).

[_Scala exercises_](http://scala-exercises.47deg.com/index.html). A collection of small exercises for different Scala features.

[_Infoq_](http://www.infoq.com). A popular tech portal with a good coverage on [functional programming](http://www.infoq.com/fp/), [Scala](http://www.infoq.com/scala/) and [Spark](http://www.infoq.com/spark).





