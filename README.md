# Programación Funcional en Scala

Este repositorio contiene el material utilizado durante el curso de programación funcional en Scala impartido por Habla Computing para el proyecto Ninja de BBVA.

En este documento también se recogen instrucciones relativas a la instalación de software, y otros aspectos necesarios para el correcto seguimiento del curso.

## Localización

El curso tendrá lugar en el edificio Vaguada.

## Contenido

Las explicaciones durante las sesiones del curso alternarán el uso de transparencias y ejemplos de programación "en vivo". Las transparencias estarán disponibles en los siguientes enlaces al comienzo de cada sesión. El curso se divide en cinco sesiones de cuatro horas que tendrán lugar por la tarde de 15h a 19:30h (con media hora de descanso, aproximadamente, entre medias).

* MODULE 1. [Introduction to Scala](tema1-scalaintro/IntroduccionAScala.pdf) (Monday, March 5th)
  * Scala & object-oriented programming
  * Scala & functional programming
  * Scala & generic programming

* MODULE 2. [Algebraic data types & functions](tema2-hofs/HOFsAndADTs.pdf) (Tuesday, March 6th)
  * The modularity ladder
  * Algebraic data types
  * Higher-order functions & catamorphisms

* MODULE 3. [Type classes](tema3-typeclasses/TypeClasses.pdf) (Wednesday, March 7th)
  * Type classes & object-oriented patterns
  * Representing data types with type classes
  * Type constructor classes: functors

* MODULE 4. [Functional architectures](tema4-languages/languages.pdf) (Monday, March 12th)
  * Functional architectures vs. conventional architectures
  * Functional & monadic APIs
  * Combining effects with type classes

* MODULE 5. Functional spark (Tuesday, March 13th)
  * The functional architecture of the Spark framework
  * The DSL: RDD transformations
  * The interpreter: RDD actions and the Spark cluster


* [Referencias](REFS.md)

Tanto el código "en vivo" como el historial de la consola de comandos se encontrarán también disponibles en este repositorio al término de cada sesión.

## Réplica y clonación del repositorio

Para facilitar las correcciones a los ejercicios propuestos durante el curso se recomienda hacer un [fork](https://help.github.com/articles/fork-a-repo/#fork-an-example-repository) de este repositorio en la cuenta Github del alumno, y clonar localmente vuestra propia versión del repositorio (instalando previamente [git](https://git-scm.com/)).

En este [documento](InstruccionesGithub.pdf) se explican paso a paso las actividades de configuración de vuestro repositorio, así como las operaciones de git que utilizaréis más comúnmente y el procedimiento para solicitar la corrección de ejercicios.

## Instalación de software

#### Librerías de Scala y compilador

Prerrequisitos: es necesario que tengáis instalado Java en vuestra máquina. Recomendamos JDK (o JRE) versión 7 u 8.

Este repositorio contiene una copia de `sbt`, la herramienta de builds de Scala más común. Una vez clonado localmente el repositorio, mediante el siguiente comando se descargará el compilador de Scala:

```bash
$ cd funcourse-ninja-bbva
$ ./sbt update
```

#### Editores

Con respecto al editor, durante las sesiones del curso utilizaremos el editor [Sublime](http://www.sublimetext.com/), pero, por supuesto, podéis utilizar cualquier otro editor (Atom, VI, emacs) o IDE (eclipse, intellij, etc. - véase el paso 3 [aquí](http://www.scala-lang.org/download/)) de vuestra elección.

## Comunicación

#### Correo electrónico

El correo electrónico del coordinador del curso es: [juanmanuel.serrano@hablapps.com](mailto:juanmanuel.serrano@hablapps.com)

#### Twitter

El hashtag "oficial" del curso es el siguiente: `#funcourseinscala`
