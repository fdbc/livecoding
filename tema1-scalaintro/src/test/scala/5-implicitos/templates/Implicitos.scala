package org.hablapps.scalaintro.implicitos
package templates

import org.scalatest._

/**
 * El primer uso que tienen los implicitos, es el paso de argumentos,
 * esto nos permite que el compilador busque por nosotros los argumentos
 * adecuados que pasarle a una función.
 */
class ArgumentosImplicitos extends FlatSpec with Matchers {

  // 1. Empezamos con una función normal y corriente
  object Step1{
    def post(data: Array[Byte], uri: String, port: Int): String =
      s"Posting to $uri on port $port"
  }

  "Implícitos: Step 1" should "work" in {
    import Step1._

  }

  // 2. Podemos marcar los argumentos que queramos como implícitos,
  // estos argumentos tienen que ir en un grupo a parte, y además
  // este grupo tiene que ser el último siempre.
  object Step2{
    // def post(data: Array[Byte], uri: String, port: Int): String =
    //   s"Posting to $uri on port $port"
  }

  // Podemos utilizar este método pasando argumentos con total
  // normalidad
  "Implícitos: Step 2" should "work" in {
    import Step2._

  }

  // 3. Pero además, en este caso, podemos marcar valores como implícitos
  // para que sean "usados" automáticamente por el método
  object Step3{
    // val uri: String = "localhost"
    // val port: Int = 8080
  }

  "Implícitos: Step3" should "work" in {
    import Step2._, Step3._


  }

  // 4. También podemos elegir qué argumentos queremos que el compilador
  // "consiga" implícitamente, haciendo uso del método `implicitly`.
  object Step4{
    // val port: Int = 8080
  }

  "Implícitos: Step4" should "work" in {
    import Step2._, Step4._

  }
}

/**
 * El otro caso de uso que nos proporcionan los implícitos son las
 * conversiones, el compilador será capaz de transformarnos un valor
 * en otro cualquiera de manera automática. Ahora veremos para qué
 * puede ser útil este comportamiento.
 */
class ConversionesImplicitas extends FlatSpec with Matchers{
  import scala.language.implicitConversions

  // 5. Partimos de una funcion de conversión ordinaria
  object Step5{
    def doubleToInt(i: Double): Int = i.toInt
  }

  "Conversiones implícitas: Step 5" should "work" in {
    import Step5._
  }

  // 7. Podemos ahorrarnos esta conversión manual, marcando al
  // método conversor como `implicit`
  object Step6{
    // def doubleToInt(i: Double): Int = i.toInt
  }

  "Conversiones implícitas: Step 6" should "work" in {
    import Step6._
  }

  // Esta práctica, sin embargo, es bastante peligrosa, puesto que
  // puede ocultar bugs en nuestro código, y en general, no es un
  // caso de uso recomendado.

  // 8. Un caso de uso más común (y más correcto) para conversiones
  // implícitas, es el aumento de funcionalidad para un tipo, por
  // ejemplo vamos a extender la funcionalidad de Int para poder
  // hacer factoriales y potencias. La solución más común es crearnos
  // un wrapper y añadir esa funcionalidad
  object Step7{

    class RichInt(i: Int) {
      def factorial: Int =
        if (i > 1)
          i * new RichInt(i-1).factorial
        else
          i
      def squared: Int = math.pow(i, 2).toInt
      def exp(e: Int): Int = math.pow(i, e).toInt
    }

  }

  "Conversiones implícitas: Step 7" should "work" in {
    import Step7._
  }

  // 8. Sin embargo, las conversiones implícitas nos permiten
  // abstraernos del wrapper y utilizar los métodos directamente
  object Step8{

    class RichInt(i: Int) {
      def factorial: Int =
        if (i > 1)
          i * new RichInt(i-1).factorial
        else
          i
      def squared: Int = math.pow(i, 2).toInt
      def exp(e: Int): Int = math.pow(i, e).toInt
    }

    // def toRichInt(i: Int) = new RichInt(i)
  }

  "Conversiones implícitas: Step 8" should "work" in {
    import Step8._

  }

  // 9. Este patrón es tan común que para eso existen otro tipo de
  // implícitos, las `implicit clases`, cuyo caso de uso es concretamente
  // ese, extender la funcionalidad de los tipos.

  object Step9{
    // class RichInt(i: Int) {
    //   def factorial: Int =
    //     if (i > 1)
    //       i * new RichInt(i-1).factorial
    //     else
    //       i
    //   def squared: Int = math.pow(i, 2).toInt
    //   def exp(e: Int): Int = math.pow(i, e).toInt
    // }
  }

  "Conversiones implícitas: Step 9" should "work" in {
    import Step9._
  }
}
