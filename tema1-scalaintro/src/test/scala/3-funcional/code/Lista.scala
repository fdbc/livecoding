package org.hablapps.scalaintro.funcional
package code

// 1. Crear estructura propia para poder trabajar con listas.
object Step1{
  abstract class Lista
  class Cons(head: Int, tail: Lista) extends Lista
  class Fin() extends Lista
}

// 2. Sellamos y cambiamos las clases por "case" clases
object Step2{
  sealed abstract class Lista
  case class Cons(head: Int, tail: Lista) extends Lista
  case class Fin() extends Lista
}

// 3. Añadimos un método para insertar un nuevo elemento en la lista (por la
// cabeza)
object Step3{
  sealed abstract class Lista{
    def insertar(head: Int): Lista = new Cons(head, this)
  }
  case class Cons(head: Int, tail: Lista) extends Lista
  case class Fin() extends Lista
}

// 4. Pattern matching. Añadimos el método `suma`, que suma todos los elementos de la lista,
// o devuelve 0 en caso de que la lista sea vacía.
object Step4{
  sealed abstract class Lista{
    def insertar(head: Int): Lista = new Cons(head, this)
    def suma: Int = this match {
      case Cons(head, tail) => head + tail.suma
      case Fin() => 0
    }
  }
  case class Cons(head: Int, tail: Lista) extends Lista
  case class Fin() extends Lista
}

// 4. Functions. Añadimos el método `map` que recibe una función (Function1[Int,Int]) para mapear
// todos los elementos de esta lista.
object Step5{
  sealed trait Lista {
    def insertar(head: Int): Lista = new Cons(head, this)
    def suma: Int = this match {
      case Cons(head, tail) => head + tail.suma
      case Fin() => 0
    }
    def map(f: Function1[Int, Int]): Lista = this match {
      case Cons(head, tail) => Cons(f(head), tail.map(f))
      case Fin() => Fin()
    }
  }
  case class Cons(head: Int, tail: Lista) extends Lista
  case class Fin() extends Lista
}