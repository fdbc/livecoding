name := "funcourseinscala"

// scalaOrganization := "org.typelevel"

scalaBinaryVersion := "2.12"

scalaVersion := "2.12.0"

organization := "org.hablapps"

addCompilerPlugin("org.spire-math" %% "kind-projector" % "0.9.3")

libraryDependencies ++= Seq(
  "org.scalatest" %% "scalatest" % "3.0.1"
)

scalacOptions ++= Seq(
  "-Xlint",
  "-unchecked",
  "-deprecation",
  "-feature",
  "-Ypartial-unification",
  // "-Xprint:typer",
  // "-Xlog-implicits",
  "-language:postfixOps",
  "-language:higherKinds")

initialCommands in console := """
  | import org.hablapps.scalaintro.funcional.templates._
  |""".stripMargin
