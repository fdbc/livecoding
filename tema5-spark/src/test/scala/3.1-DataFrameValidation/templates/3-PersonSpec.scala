package org.hablapps.fpinscala.spark
package dataframevalidation
package templates

import org.apache.spark.sql._, expressions._, types._, functions._

import org.scalatest._
import com.holdenkarau.spark.testing.DataFrameSuiteBase

class ErrorHandlingSpec extends FunSpec with DataFrameSuiteBase{

  override def beforeAll() {
    super.beforeAll()
    spark.conf.set("spark.sql.shuffle.partitions", 2)
  }

  import spark.implicits._
  import Validated.Syntax._

  // Validate

  describe("Person dataframe"){
    import Person.ValidatedPerson._

    val personSeq = Seq(
      Person("pepe", 40),
      Person("isa", -20),
      Person(null, 30),
      Person("", -35))

    def personDF = personSeq.toDF

    ignore("should be validated"){

      println("ORIGINAL DATASET")
      personDF.show

      println("VALIDATED DATASET")
      personDF.validate[Person].show

      assertDataFrameEquals(
        personDF.validate[Person],
        ???)
    }

    ignore("should be filtered with valid rows"){

      println("VALID ROWS")
      personDF.validate[Person].filterValid[Person].show

      assertDataFrameEquals(
        personDF.validate[Person].filterValid[Person],
        ???)
    }

    ignore("should be filtered with invalid rows"){

      println("INVALID ROWS")
      personDF.validate[Person].filterInvalid[Person].show

      assertDataFrameEquals(
        personDF.validate[Person].filterInvalid[Person],
        ???)
    }

    ignore("should be filtered with invalid rows for given errors"){

      println("INVALID ROWS (for emptyName and negativeAge errors)")
      personDF.validate[Person].filterAllInvalid[Person]("emptyName", "negativeAge").show

      assertDataFrameEquals(
        personDF.validate[Person].filterAllInvalid[Person]("emptyName", "negativeAge"),
        ???)
    }
  }
}
