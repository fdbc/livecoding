package org.hablapps.fpinscala.spark
package dataframevalidation
package templates

import org.apache.spark.sql._, expressions._, types._, functions._

case class Person(name: String, age: Int)

object Person{

  implicit object ValidatedPerson extends Validated[Person]{
    import Encoders.product

    // Schema
    lazy val Schema = ???

    // Errors

    sealed abstract class Error

    object Error extends Validated.ErrorCompanion[Error]{

      case class Repr()

      lazy val Schema = ???
    }

    // Validations
    import Validated.Syntax._

    lazy val validations = {
      lazy val emptyName: UserDefinedFunction = ???

      lazy val negativeAge: UserDefinedFunction = ???

      lazy val notDefinedName: UserDefinedFunction = ???

      lazy val notDefinedAge: UserDefinedFunction = ???

      init + ???
    }
  }
}