package org.hablapps.fpinscala.spark
package dataframevalidation

import org.apache.spark.sql._, expressions._, types._, functions._

case class Person(name: String, age: Int)

object Person{

  implicit object ValidatedPerson extends Validated[Person]{
    import Encoders.product

    // Schema
    val Schema = product[Person].schema

    // Errors

    sealed abstract class Error
    case class NotDefinedName() extends Error
    case class NotDefinedAge() extends Error
    case class NegativeAge(wrongAge: Int) extends Error
    case class EmptyName() extends Error

    object Error extends Validated.ErrorCompanion[Error]{

      case class Repr(notDefinedName: NotDefinedName,
        notDefinedAge: NotDefinedAge,
        negativeAge: NegativeAge,
        emptyName: EmptyName)

      val Schema = product[Repr].schema
    }

    // Validations
    import Validated.Syntax._

    val validations = {
      val emptyName = udf((name: String) =>
        if (name != null && name == "") EmptyName() else null)

      val negativeAge = udf((age: Int) =>
        if (age != null && age < 0) NegativeAge(age) else null)

      val notDefinedName = udf((value: Any) =>
        if (value == null) NotDefinedName() else null)

      val notDefinedAge = udf((value: Any) =>
        if (value == null) NotDefinedAge() else null)

      init +
        validation("notDefinedName", "name", notDefinedName) +
        validation("notDefinedAge", "age", notDefinedAge) +
        validation("negativeAge", "age", negativeAge) +
        validation("emptyName", "name", emptyName)
    }
  }
}