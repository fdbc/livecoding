package org.hablapps.fpinscala.spark
package templates

import org.scalatest._
import com.holdenkarau.spark.testing.{RDDGenerator, SharedSparkContext}

class PageRankSpec extends FunSpec with Matchers with SharedSparkContext{

  import org.apache.spark.rdd.RDD

  import org.scalactic._

  val doubleEquality = TolerantNumerics.tolerantDoubleEquality(0.1)

  def tolerantMapDoubleEquality(tolerance: Double): Equality[Map[String,Double]] = {
    if (tolerance <= 0.0)
      throw new IllegalArgumentException(tolerance.toString + " passed to tolerantDoubleEquality was zero or negative. Must be a positive non-zero number.")
    new Equality[Map[String,Double]]{
      def areEqual(a: Map[String,Double], b: Any): Boolean = {
        b match {
          case b: Map[String,Double]@unchecked =>
            a.forall{ case (k,v1) =>
              b.get(k).fold(false)(v2 => doubleEquality.areEqual(v1,v2))
            } &&  a.size == b.size
          case _ => false
        }
      }
      override def toString: String = s"TolerantMapDoubleEquality($tolerance)"
    }
  }

  implicit val mapOfDoublesEquality = tolerantMapDoubleEquality(0.01)

  ignore("PageRank"){
    // Examples taken from http://www.cs.princeton.edu/~chazelle/courses/BIB/pagerank.htm
    import RemovingVarsFromRDDs.UsingScalaIterator.pageRank

    it("should work for example1"){
      val graph = Map(
        "A"->List("B","C"),
        "B"->List("C"),
        "C"->List("A"),
        "D"->List("C"))

      pageRank(sc.parallelize(graph.toSeq)).collect().toMap shouldEqual
        Map("A"->1.49, "B"->0.78, "C"->1.58, "D"->0.15)
    }

    it("should work for example2"){
      val graph = Map(
        "A"->List("B","C","D"),
        "B"->List("A"),
        "C"->List("A"),
        "D"->List("E","F","G","H","A"),
        "E"->List(),
        "F"->List(),
        "G"->List(),
        "H"->List())

      pageRank(sc.parallelize(graph.toSeq)).collect().toMap shouldEqual
        Map("A"->0.92, "B"->0.41, "C"->0.41, "D"->0.41,
            "E"->0.22, "F"->0.22, "G"->0.22, "H"->0.22)
    }

    it("should work for example3"){
      val graph = Map("1"->List(), "2"->List(), "3"->List())

      pageRank(sc.parallelize(graph.toSeq)).collect().toMap shouldBe
        Map("1"->0.15, "2"->0.15, "3"->0.15)
    }

  }

}