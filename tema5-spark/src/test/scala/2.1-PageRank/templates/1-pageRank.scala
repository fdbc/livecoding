package org.hablapps.fpinscala.spark
package templates

object RemovingVarsFromRDDs{

  import org.apache.spark.rdd.RDD

  // PageRank algorithm implemented with 'vars'

  object WithPlainVars{

    def pageRank(graph: RDD[(String,Seq[String])]): RDD[(String,Double)] = {
      var ranks: RDD[(String,Double)] = graph.mapValues(_ => 1.0)

      for (i <- 0 until 10){
        val contributions: RDD[(String, Double)] =
          graph.join(ranks).flatMap{
            case (node, (links,rank)) =>
              links.map(dest => (dest, rank/links.size))
          }.reduceByKey(_ + _)
        ranks = ranks.leftOuterJoin(contributions)
                  .mapValues{ case (_,mayBeCont) =>
                    0.15 + 0.85*mayBeCont.fold(0.0)(identity)
                  }
      }

      ranks
    }
  }

  object WithCustomIterate{

    def iterate[T](times: Int)(initial: T)(update: T => T): T = ???

    def pageRank(graph: RDD[(String,List[String])]): RDD[(String,Double)] = ???
  }

  object UsingScalaIterator{

    class IteratorOps

    def pageRank(graph: RDD[(String,List[String])]): RDD[(String,Double)] = ???
  }

}