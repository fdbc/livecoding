package org.hablapps.fpinscala.spark

import org.scalatest._

import org.apache.spark.rdd.RDD

object OrderedRDDs extends OrderedRDDs

class OrderedRDDs extends FunSpec with Matchers with SparkSetUpAndStop {

  /**
   * Parte I
   * Transformaciones sobre RDDs de pares con tipo K ordenado
   * (OrderedRDDFunctions)
   */
  lazy val users: RDD[(Int,String)] = sc.parallelize(List(
    (2,"Pepe"),
    (1,"Ana"),
    (4,"Javier"),
    (3,"Rosa"),
    (5,"Maria")))

  // filtrado por rango
  describe("OrderedRDDs"){
    it("Filtrado por rango 1-3"){
      val users1To3: RDD[(Int, String)] = users.filterByRange(1, 3)
      users1To3.collectAsMap shouldBe Map((1,"Ana"),
        (2,"Pepe"),
        (3,"Rosa"))
    }

    it("ordenación por clave"){
      val usersSorted: RDD[(Int, String)] = users.sortByKey()
      usersSorted.collectAsMap shouldBe Map((2,"Pepe"),
        (1,"Ana"),
        (4,"Javier"),
        (3,"Rosa"),
        (5,"Maria"))
    }
  }
}
