package org.hablapps.fpinscala.spark

import org.scalatest._

import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf
import org.apache.spark.rdd.RDD
import org.apache.spark.storage.StorageLevel

object CoreRDDs extends CoreRDDs

class CoreRDDs extends FunSpec with Matchers with SparkSetUpAndStop{

  /*
   * TRANSFORMACIONES
   */

  // Leemos las lineas de un archivo de texto
  lazy val lines: RDD[String] = sc.textFile("data/quijote.txt")

  // Separamos las líneas por palabras
  lazy val words: RDD[String] = lines.flatMap(_.split(" "))

  // Filtramos las palabras vacías
  lazy val nonEmptyWords: RDD[String] = words.filter(!_.isEmpty)

  // Aplicamos una transformación para quedarnos únicamente con la longitud de las palabras
  lazy val wordsLength: RDD[Int] = nonEmptyWords.map(_.length)

  /*
   * ACCIONES
   */
  describe("Actions"){
    it("should work"){

      // ¿Cuántas palabras hay?

      val wordsNumber: Long = words.count

      wordsNumber shouldBe 59510

      // Palabras

      val wordsMat: Array[String] = words.collect

      wordsMat.slice(0,2) shouldBe Array("EL", "INGENIOSO")

      // ¿Cuáles son sus tamaños?

      val wordsLengthMat: Array[Int] = wordsLength.collect

      wordsLengthMat.slice(0,5) shouldBe Array(2,9,7,3,7)

      // ¿Cuáles son las longitudes de las 5 primeras palabras?

      val firstFiveWords: Array[Int] = wordsLength take 5

      firstFiveWords shouldBe Array(2,9,7,3,7)

      // ¿Y de la primera?

      val firstWord: Int = wordsLength.first

      firstWord shouldBe 2

      // ¿Está vacío el conjunto?

      val isEmpty: Boolean = wordsLength.isEmpty

      isEmpty shouldBe false

    }
  }

  /*
   * TRANSFORMACIONES SOBRE CONJUNTOS
   */

  // Ahora vamos a ver algunas transformaciones con conjuntos

  lazy val dice: RDD[Int] = sc.parallelize(1 to 6)

  lazy val oddDice: RDD[Int] = dice.filter(_ % 2 == 1)

  // Subtract

  lazy val evenDice: RDD[Int] = dice subtract oddDice

  // Intersection

  lazy val emptyDS: RDD[Int] = oddDice intersection evenDice

  // Union

  lazy val wholeDice: RDD[Int] = oddDice union evenDice

  describe("Actions of set transformed RDDs"){
    it("should work"){

      dice.collect shouldBe Array(1,2,3,4,5,6)

      oddDice.collect shouldBe Array(1,3,5)

      evenDice.collect shouldBe Array(2,4,6)

      emptyDS.collect shouldBe Array()

      wholeDice.collect shouldBe Array(1,3,5,2,4,6)
    }
  }

  /*
   * AGGREGATE, DE LAS TRANSFORMACIONES MÁS POTENTES
   */

  lazy val randomNumbers: RDD[Double] = sc.parallelize(1 to 1000).map(_ => math.random * 1000)

  describe("Aggregate actions"){

    it("like aggregate should work"){
      lazy val LengthAndSum: (Int,Double) =
        randomNumbers.aggregate[(Int, Double)]((0, 0.0))(
          (acc, d) => (acc._1 + 1, acc._2 + d),
          (t1, t2) => (t1._1 + t2._1, t1._2 + t2._2))

      LengthAndSum._1 shouldBe 1000
    }

    it("like reduce and fold should work"){
      // ¿Cuantas letras hay en total?

      val charsNumber: Int = wordsLength.reduce(_ + _)

      charsNumber shouldBe 251601

      // De una manera más segura

      val charsNumberSafer: Int = wordsLength.fold(0)(_ + _)

      charsNumberSafer shouldBe 251601
    }

  }

  /*
   * GROUPBY Y SORTBY
   */

  describe("GroupBy y SortBy transformations"){

    // sample dataset
    lazy val simpleCol: RDD[Int] = sc.parallelize(Array(22, 42, 31, 41, 1, 6))

    it("should work"){
      val sortedDS: RDD[Int] =
        simpleCol.sortBy(identity)

      sortedDS.collect shouldBe Array(1,6,22,31,41,42)

      val byEndings: RDD[(Int, Iterable[Int])] =
        simpleCol.groupBy(_.toInt % 10)

      byEndings.collect.toMap.apply(1) shouldBe
        Array(31,41,1)
    }
  }

}
