package org.hablapps.fpinscala.spark

import org.scalatest._

object Intro extends Intro

class Intro extends FunSpec with Matchers with SparkSetUpAndStop{

  // We set up a new RDD from a custom in-memory collection
  import org.apache.spark.rdd.RDD

  // We set up a new RDD from a custom in-memory collection
  // val dice: RDD[Int] = sc.parallelize(1 to 6)
  lazy val dice: RDD[Int] = sc.parallelize(1 to 6)

  // We create a new RDD by transforming the previous one
  lazy val oddDice: RDD[Int] = dice.filter(_ % 2 != 0)

  // Tests

  describe("Materialize data"){

    it("Odd dice dataset should be ok"){
      // We execute this transformation through `collect`
      oddDice.collect shouldBe Array(1,3,5)
    }

    it("Whole dice dataset should be ok"){
      // If no transformation is applied, the materialized data set should be the same
      dice.collect shouldBe Array(1,2,3,4,5,6)
    }

  }

}
