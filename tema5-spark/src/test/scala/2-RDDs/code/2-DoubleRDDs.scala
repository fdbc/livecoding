package org.hablapps.fpinscala.spark

import org.scalatest._

object DoubleRDDs extends DoubleRDDs

class DoubleRDDs extends FunSpec with Matchers with SparkSetUpAndStop {
  import org.apache.spark.rdd.RDD

  lazy val sampleRDD: RDD[Int] = sc.parallelize(List(1,2,3,4))

  describe("Double actions"){

    it("should work"){
      // Calculamos la suma
      val sum: Double = sampleRDD.sum

      sum shouldBe 10

      // Calculamos la media
      val mean: Double = sampleRDD.mean

      mean shouldBe 2.5

      // Calculamos la varianza
      val variance: Double = sampleRDD.variance

      variance shouldBe 1.25

      // Varianza a mano

      val (length, sumDiff): (Int,Double) =
        sampleRDD.map(e => Math.pow(e - mean,2))
          .aggregate((0,0.0))({
            case ((l,s),e) => (l+1, s+e)
          }, {
            case ((l1,s1), (l2,s2)) => (l1+l2, s1+s2)
          })

      sumDiff/length shouldBe variance

    }
  }

}
