package org.hablapps.fpinscala.spark
package section2
package templates

import org.scalatest._

import org.apache.spark.rdd.RDD

object OrderedRDDs extends OrderedRDDs

class OrderedRDDs extends FunSpec with Matchers with SparkSetUpAndStop {

  /**
   * Parte I
   * Transformaciones sobre RDDs de pares con tipo K ordenado
   * (OrderedRDDFunctions)
   */
  lazy val users: RDD[(Int,String)] = sc.parallelize(List(
    (2,"Pepe"),
    (1,"Ana"),
    (4,"Javier"),
    (3,"Rosa"),
    (5,"Maria")))

  // filtrado por rango
  describe("OrderedRDDs"){
    ignore("Filtrado por rango 1-3"){
      val users1To3: RDD[(Int, String)] = ???
    }

    ignore("ordenación por clave"){
      val usersSorted: RDD[(Int, String)] = ???
    }
  }

}
