package org.hablapps.fpinscala.spark
package section2
package templates

import org.scalatest._

import org.apache.spark.rdd.RDD

object PairRDDs extends PairRDDs

class PairRDDs extends FunSpec with Matchers with SparkSetUpAndStop {

  lazy val bills: RDD[(String, Int)] =
    sc.parallelize(List(
      ("Luis",  5),
      ("Pepe", 20),
      ("Jose", 30),
      ("Pepe", 10),
      ("Jose",  5)))

  /**
   * Parte I
   * Acciones sobre pares de RDDs (PairRDDsFunctions)
   */

  describe("Inspecting Pair RDDs"){

    ignore("should work"){
      // cuenta de valores por clave

      lazy val billsPerUser: scala.collection.Map[String, Long] = ???

      billsPerUser shouldBe ???

      // retorna los valores para una clave dada

      lazy val joseBillsValues: Seq[Int] =
        ???

      joseBillsValues shouldBe ???
    }
  }

  /**
   * Parte II
   * Transformaciones sobre pares de RDDs (PairRDDFunctions)
   */
  describe("Pair transformations"){
    ignore("should work"){

      // transformación de valores

      lazy val amountsWithVAT: RDD[(String, Double)] =
        ??? // Asumiendo IVA 20%

      amountsWithVAT.collect shouldBe
        ???

      // agrupación de valores por clave

      lazy val amountsPerUser: RDD[(String, Iterable[Int])] =
        ???

      amountsPerUser lookup "Pepe" shouldBe
        ???

      // combinación de valores por clave, sin valor zero (neutro). Obliga a que el tipo retornado sea el de los valores del RDD

      lazy val fullAmountPerUser: RDD[(String, Int)] =
        ???

      fullAmountPerUser lookup "Pepe" shouldBe
        ???

      // Esta vez de manera segura

      lazy val fullAmountPerUserSafer: RDD[(String, Int)] =
        ???

      fullAmountPerUserSafer.collectAsMap()("Pepe") shouldBe
        (??? : Int)

    }
  }

  /**
   * Parte III
   * Transformaciones sobre dos pares de RDDs (PairRDDFunctions),
   * son operaciones de conjuntos
   */
  lazy val addresses: RDD[(String, String)] =
    sc.parallelize(("Luis", "Avd Alamo 5") :: ("Jose", "Calle Pez 12") :: Nil)

  describe("Two pair transforms."){
    ignore("should work"){
      // sustraer por clave
      lazy val usersWithNoAddress: RDD[(String, Int)] =
        ???

      usersWithNoAddress.map(_._1).distinct.collect shouldBe
        ???

      // cogroup, aúna todos los valores por clave en ambos RDDs.
      // Si una clave no está en uno de los RDDs el Iterable correspondiente estará vacío
      lazy val usersBillsAndAddress: RDD[(String,(Iterable[Int], Iterable[String]))] =
        ???

      usersBillsAndAddress lookup "Jose" shouldBe
        ???

      // (inner) join, devuelve las keys que están en ambos RDDs, con sus valores. Si una clave está
      // repetida en alguno de los RDDs se hace producto cartesiano sobre los valores en ambos RDDs
      lazy val billsWithAddress: RDD[(String, (Int, String))] =
        ???

      billsWithAddress lookup "Jose" shouldBe
        ???
    }
  }


}
