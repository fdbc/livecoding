package org.hablapps.fpinscala.spark
package section2
package templates

import org.scalatest._

object Intro extends Intro

class Intro extends FunSpec with Matchers with SparkSetUpAndStop{

  /**
   * INTRO
   */

  import org.apache.spark.rdd.RDD

  // We set up a new RDD from a custom in-memory collection
  
  // We create a new RDD by transforming the previous one

  // Tests

  describe("Materialize data"){

    ignore("Odd dice dataset should be ok"){
      // We execute this transformation through `collect`
    }

    ignore("Whole dice dataset should be ok"){
      // If no transformation is applied, the materialized data set should be the same
    }

  }

  /**
   *  CACHING
   */

  // We set up a new RDD from an external text file

  // Cached buildsbt


  describe("Uncached datasets"){
    import scala.io.StdIn._

    ignore("should reflect changes"){
      // The number of lines should be 36


      // Add a line to the file and press any key (uncached test)


      // The result should be different

    }
  }

  describe("Cached datasets"){
    import scala.io.StdIn._

    ignore("should work ok"){
      // In the first action, the result is cached


      // Remove a line from the file and press any key (cached test)


      // The result should be the same

    }

    ignore("also affects uncached RDDs (!)"){

    }
  }
}
