package org.hablapps.fpinscala.spark
package section2
package templates

import org.scalatest._

object DoubleRDDs extends DoubleRDDs

class DoubleRDDs extends FunSpec with Matchers with SparkSetUpAndStop {
  import org.apache.spark.rdd.RDD

  lazy val sampleRDD: RDD[Int] = sc.parallelize(List(1,2,3,4))

  describe("Double actions"){

    ignore("should work"){
      // Calculamos la suma
      val sum: Double = ???

      sum shouldBe (??? : Double)

      // Calculamos la media
      val mean: Double = ???

      mean shouldBe (??? : Double)

      // Calculamos la varianza
      val variance: Double = ???

      variance shouldBe (??? : Double)

      // Varianza a mano

      val (length, sumDiff): (Int,Double) = ???

      sumDiff/length shouldBe variance

    }
  }

}
