package org.hablapps.fpinscala

import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf
import org.apache.spark.rdd.RDD
import org.apache.spark.storage.StorageLevel

import org.scalatest._

package object spark{

  trait SparkSetUpAndStop extends BeforeAndAfterAll{ this: Suite =>
    // Before all, we create the Spark context

    import org.apache.spark.{SparkConf, SparkContext}

    var sc: SparkContext = null

    override def beforeAll {
      val conf = new SparkConf()
        .setMaster("local")
        .setAppName("Intro")
      sc = new SparkContext(conf)
    }

    /**
     * Parte II
     * Creación RDDs simples
     */

    object SampleRDDs{

      // Básica (hardcoded)

      lazy val dice: RDD[Int] = sc.parallelize(1 to 6)

      lazy val randomVals = for {
        _ <- (1 to 1000)
      } yield (math.random * 1000).toInt

      lazy val randomNumbers: RDD[Int] = sc.parallelize(randomVals)

      // A partir de ficheros

      lazy val numsLocal: RDD[String] = sc.textFile("data/NumsLocal.txt")


      lazy val quijote: RDD[String] = sc.textFile("data/quijote.txt")
    }

    // After tests, we shut down the Spark context

    override def afterAll{
      sc.stop
    }

  }
}