package org.hablapps.fpinscala.spark

import org.scalatest._

// Application domain

object Domain{

  case class Person(
    name: String,
    age: Int,
    gender: Option[Boolean]) extends Serializable

}

// Information source

object DataSource{
  import Domain._
  import org.apache.spark.rdd.RDD

  type DataSet = RDD[Record]

  type Record = Array[Field]

  type Field = String

  object Field{

    sealed abstract class Type(val column: Int)
    case object Name extends Type(0)
    case object Age extends Type(1)
    case object Gender extends Type(2)

    // Possible errors

    sealed abstract class Error
    case class NegativeAge(value: Int) extends Error
    case class WrongField(value: String, field: Field.Type) extends Error
    case class NotDefined(field: Field.Type) extends Error
  }
}
