package org.hablapps.fpinscala.spark
package templates

import org.scalatest._

// Application domain

object Domain{

  case class Person(
    name: String,
    age: Int,
    gender: Option[Boolean])

}

// Information source

object DataSource{
  import Domain._
  import org.apache.spark.rdd.RDD

  type DataSet = RDD[Record]

  type Record = Array[Field]

  type Field = String

  object Field{

    sealed abstract class Type

    // Possible errors

    sealed abstract class Error

  }
}
