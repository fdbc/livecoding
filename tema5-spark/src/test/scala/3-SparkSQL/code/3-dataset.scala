package org.hablapps.fpinscala.spark
package sql

import org.scalatest._
import com.holdenkarau.spark.testing.DataFrameSuiteBase

class DatasetSpec extends FunSpec with Matchers with DataFrameSuiteBase{

  import org.apache.spark.sql._
  import spark.implicits._

  describe("DatasetSpec"){
    
    lazy val cliDS: Dataset[Client] = 
      spark.read.parquet("data/clients").as[Client]

    it("should work with untyped transformations"){

      val youngClients1: Dataset[Int] = cliDS
        .filter($"age" < 30)
        .select($"id".as[Int])

      youngClients1.explain

      youngClients1.collect.toSet shouldBe Set(5, 4, 1)
    }

    it("shouldn't work when misspelling fields, but it does"){

      // This shouldn't compile
      lazy val youngClients2: Dataset[Int] = cliDS
        .filter($"aged" < 30)
        .select($"id".as[Int])

      an[Exception] should be thrownBy(youngClients2.collect)
      
      // This shouldn't compile
      lazy val youngClients3: Dataset[String] = cliDS
        .filter($"age" < 30)
        .select($"nick".as[String])
    }

    it("should work when using typed transformations, but less efficiently"){
      val youngClients4: Dataset[Int] = cliDS
        .filter(_.age < 30)
        .map(_.id)

      youngClients4.explain

      youngClients4.collect.toSet shouldBe Set(5,4,1)
    }
  }
}
