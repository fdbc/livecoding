package org.hablapps.fpinscala.spark
package sql
package templates

import org.scalatest._
import com.holdenkarau.spark.testing.DataFrameSuiteBase

class ErrorHandlingSpec extends FunSpec with DataFrameSuiteBase{

  override def beforeAll() {
    super.beforeAll()
    spark.conf.set("spark.sql.shuffle.partitions", 2)
  }

  import spark.implicits._

  describe("Create datasets"){

    it("client dataset"){
      val clients: List[Client] = List(
        Client(1, "John Doe", 27), Client(2, "John Doe", 27), Client(3, "John Doe", 27),
        Client(4, "John Doe", 27), Client(5, "John Doe", 27), Client(6, "John Doe", 27))
      
      // spark.createDataset(clients).write.parquet("data/clients")
      clients.toDS.write.parquet("data/clients")
    }

    it("transfer datasets"){
      val transfers: List[Transfer] = List(
        Transfer(1, 4), Transfer(4, 1),
        Transfer(2, 4), Transfer(4, 2),
        Transfer(3, 4), Transfer(4, 3),
        Transfer(5, 4), Transfer(4, 5),
        Transfer(6, 4), Transfer(4, 6))
      
      spark.createDataset(transfers).write.parquet("data/transfers")
    }
  }
}
