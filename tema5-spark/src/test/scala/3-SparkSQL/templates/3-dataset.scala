package org.hablapps.fpinscala.spark
package sql
package templates

import org.scalatest._
import com.holdenkarau.spark.testing.DataFrameSuiteBase

class DatasetSpec extends FunSpec with Matchers with DataFrameSuiteBase{

  import org.apache.spark.sql._
  import spark.implicits._

  describe("DatasetSpec"){
    
    lazy val cliDS: Dataset[Client] = 
      spark.read.parquet("data/clients").as[Client]

    ignore("should work with untyped transformations"){

      val youngClients: Dataset[Int] = ???

      youngClients.explain

      youngClients.collect.toSet shouldBe ???
    }

    ignore("shouldn't work when misspelling fields, but it does"){

      // This shouldn't compile
      lazy val youngClients: Dataset[Int] = ???

    }

    ignore("should work when using typed transformations, but less efficiently"){
      
      val youngClients: Dataset[Int] = ???

      youngClients.explain

      youngClients.collect.toSet shouldBe ???
    }
  }
}
