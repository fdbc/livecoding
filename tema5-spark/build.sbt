name := "funcourseinscala"

scalaBinaryVersion := "2.11"

scalaVersion := "2.11.8"

organization := "org.hablapps"

addCompilerPlugin("org.spire-math" %% "kind-projector" % "0.9.3")

libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-sql" % "2.2.0" % "provided"
    exclude("org.glassfish.hk2", "hk2-locator")
    exclude("javax.validation", "validation-api"),
  "org.typelevel" %% "frameless-dataset" % "0.3.0",
  // "org.scalaz" %% "scalaz-core" % "7.2.7",
  // // "org.scalaz" %% "scalaz-scalacheck-binding" % "7.2.7",
  "org.scalaz" %% "scalaz-scalacheck-binding" % "7.2.15",
  "org.scalatest" %% "scalatest" % "3.0.1",
  "org.apache.spark" %% "spark-hive" % "2.2.0" % "provided",
  "com.holdenkarau" %% "spark-testing-base" % "2.1.1_0.7.0" % "test"
)

// initialCommands in console := """
//   | import org.apache.spark.{SparkConf, SparkContext}
//   | import org.apache.spark.rdd.RDD
//   |""".stripMargin
