package org.hablapps.fpinscala.typeclasses
package pattern
package code

import org.scalatest._

/**
  Scala supports type classes through several mechanisms, mainly related
  with implicits.
*/
class Implicits extends towards.code.AggregationProblem{

  trait Monoid[T]{
    val zero: T
    def add(t1: T,t2: T): T
  }

  object Monoid{
    implicit val intMonoid: Monoid[Int] = new Monoid[Int]{
      val zero = 0
      def add(t1: Int, t2: Int): Int = t1 + t2
    }

    implicit object stringMonoid extends Monoid[String]{
      val zero: String = ""
      def add(t1: String, t2: String): String = t1 + t2
    }
  }

  object ImplicitParameters {

    // If we declared the monoid instance as "implicit", then we may
    // let the compiler in charge of looking for an instance of the
    // monoid.

    def collapse[A](t: List[A])(implicit monoid: Monoid[A]): A = {
      t match {
        case Nil => monoid.zero
        case h :: t =>
          monoid.add(h, collapse(t)(monoid))
      }
    }

    // The compiler will look for implicit instances in the current scope,
    // but also in the companion objects of the type involved.

    /*implicit*/ val intMonoid = Monoid.intMonoid
    /*implicit*/ val stringMonoid = Monoid.stringMonoid

    // The dependency will be injected automatically by the compiler.
    // The compiler option "-Xprint:typer" can be used to know which exact
    // instance is passed by the compiler.

    def sum(l: List[Int]): Int =
      collapse(l)

    def concat(l: List[String]): String =
      collapse(l)
  }

  describe("La version con parámetros implicitos") {
    import ImplicitParameters._

    testAggregationInt(sum)
    testAggregationString(concat)
  }

  object ContextBounds {
    import ImplicitParameters._

    // Context bounds can be used as well, but now we need to summon the
    // type class instance with `implicitly[_]`, which is cumbersome.

    def collapse[A: Monoid](t: List[A]): A = {
      val monoid = implicitly[Monoid[A]]
      t match {
        case Nil => monoid.zero
        case h :: t =>
          monoid.add(h, collapse(t)(monoid))
      }
    }

    def sum(l: List[Int]): Int =
      collapse(l)
    def concat(l: List[String]): String =
      collapse(l)
  }

  describe("La version con context bounds en parámetros genéricos") {
    import ContextBounds._

    testAggregationInt(sum)
    testAggregationString(concat)
  }

  object AvoidingImplicitly {
    import ImplicitParameters._

    // To avoid `implicitly`, we can use some syntactic sugar: helper
    // methods, possibly implemented with implicit classes for infix operators.

    object MonoidSyntax {

      // Binary ops
      implicit class InfixSyntax[A](a1: A)(implicit M: Monoid[A]){
        def add(a2: A): A = M.add(a1,a2)
      }

      // Non-binary ops
      def zero[T](implicit M: Monoid[T]): T = M.zero
    }

    // We can now write our function more concisely.
    import MonoidSyntax._

    def collapse[A: Monoid](t: List[A]): A =
      t match {
        case Nil => zero[A]
        case h :: t => h add collapse(t)
      }

    def sum(l: List[Int]): Int =
      collapse(l)
    def concat(l: List[String]): String =
      collapse(l)
  }

  describe("La version con azúcar sintáctico") {
    import AvoidingImplicitly._

    testAggregationInt(sum)
    testAggregationString(concat)
  }

}












