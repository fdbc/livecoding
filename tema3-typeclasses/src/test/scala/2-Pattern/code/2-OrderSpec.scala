package org.hablapps.fpinscala.typeclasses
package pattern
package code

import org.scalatest._

class OrderSpec extends FunSpec with Matchers {

  import Order.syntax._

  def quicksort[A: Order](l: List[A]): List[A] = l match {
    case Nil => Nil
    case h :: t =>
      val (lower, greater) = t.partition(_ < h)
      quicksort(lower) ::: h :: quicksort(greater)
  }

  def median[A](l: List[A])(implicit O: Order[A]): Option[A] = l match {
  // def median[A: Order](l: List[A]): Option[A] = l match {
    case Nil => None
    case _ =>
      val sorted = quicksort(l)
      val index =
        if (sorted.length % 2 == 0)
          sorted.length / 2
        else
          (sorted.length+1) / 2
      Option(sorted(index-1))
  }

  describe("La función quicksort") {

    it("funciona sean cuales sean los elementos de la lista") {
      quicksort(List(1, 3, -2, 4, 6, 1, 0, 3)) shouldBe List(-2, 0, 1, 1, 3, 3, 4, 6)
      quicksort(List(Option(1), Option(3), Option(-2), Option.empty[Int], Option(6), Option(1), Option(0), Option(3))) shouldBe
        List(Option.empty[Int], Option(-2), Option(0), Option(1), Option(1), Option(3), Option(3), Option(6))
    }
  }

  describe("La función median") {

    it("funciona sean cuales sean los elementos de la lista") {
      median(List(1, 3, -2, 4, 6, 1, 0, 3)) shouldBe Option(1)
      median(List(1, 3, -2, 4, 6, 1, 0)) shouldBe Option(1)
      median(List.empty[Int]) shouldBe Option.empty[Int]

      median(List(Option(1), Option(3), Option(-2), Option.empty[Int], Option(6), Option(1), Option(0), Option(3))) shouldBe
        Option(Option(1))
      median(List(Option(1), Option(3), Option(-2), Option.empty[Int], Option(6), Option(1), Option(0))) shouldBe
        Option(Option(1))
      median(List.empty[Option[Int]]) shouldBe Option.empty[Option[Int]]
    }
  }

}
