package org.hablapps.fpinscala.typeclasses
package test

import _root_.org.scalatest._
import Exercise1._

class Exercise1Spec extends FunSpec with Matchers {

  val l1 = 1 :: 4 :: 3 :: 5 :: 2 :: Nil
  val l2 = "1" :: "4" :: "3" :: "5" :: "2" :: Nil
  val l3 = Option(1) :: Option(4) :: Option(3) :: Option(5) :: Option(2) :: Nil
  val l4 = List.empty[Int]

  describe("writeFirstUnsafe"){
    ignore("should work with non-empty Int lists"){
      Exercise1.writeFirstUnsafe(l1) shouldBe "1"
    }

    ignore("should work with non-empty String lists"){
      Exercise1.writeFirstUnsafe(l2) shouldBe "1"
    }

    ignore("should work with non-empty Option[Int] lists"){
      Exercise1.writeFirstUnsafe(l3) shouldBe "Some(1)"
    }

    ignore("should fail with an empty Int list"){
      a [NoSuchElementException] should be thrownBy
        Exercise1.writeFirstUnsafe(l4)
    }
  }

  describe("writeFirstSafe"){
    ignore("should work with non-empty Int lists"){
      Exercise1.writeFirstSafe(l1) shouldBe Some("1")
    }

    ignore("should work with non-empty String lists"){
      Exercise1.writeFirstSafe(l2) shouldBe Some("1")
    }

    ignore("should work with non-empty Option[Int] lists"){
      Exercise1.writeFirstSafe(l3) shouldBe Some("Some(1)")
    }

    ignore("should work with an empty Int list"){
      Exercise1.writeFirstSafe(l4) shouldBe None
    }
  }
}
