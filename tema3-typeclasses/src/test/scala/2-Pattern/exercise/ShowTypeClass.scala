package org.hablapps.fpinscala.typeclasses

/**
 * We've defined a type class that allow us to give string representations for values of a 
 * given type. Implement both the instances and the syntax of the type class, as well as 
 * additional functions over the type class.
 *
 * test this exercise with the following alias `test-typeclasses-exercise1`
 */
object Exercise1 {

  trait Show[A] {
    def write(a: A): String
  }

  trait ShowInstances {
    // Part I: Instances for the type class Show[_]
    // Summoner
    ???

    // `Int` instance
    implicit val intInstance: Show[Int] = ???

    // `String` instance
    ???

    // `Option[A]` instance, building upon an instance `Show[A]`
    ???
  }

  trait ShowSyntax {
    // Part II: Syntactic sugar
    // (Note): you can implement either an operator, or normal function for
    // the `write` operation
    object syntax {
      ???
    }
  }

  object Show extends ShowInstances with ShowSyntax

  // Part III: Sample programs that use the `Show` type class.
  import Show.syntax._

  // This function returns the head of the list as an String
  // (Note): The signature of this function must be modified!
  def writeFirstUnsafe[A](l: List[A]): String = ???

  // The last function was not type safe. Implement this improved
  // version.
  // (Note): The signature of this function must be modified!
  def writeFirstSafe[A](l: List[A]): Option[String] = ???

}
