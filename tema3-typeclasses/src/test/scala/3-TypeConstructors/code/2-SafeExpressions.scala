package org.hablapps.fpinscala.typeclasses
package constructors
package code

object SafeExpressions {

  object UnsafePrograms {

    /**
     * The previous implementation of arithmetic expressions does not
     * prevent us from writing malformed expressions.
     */
    object WithADTs{

      sealed trait Exp
      case class Lit(x: Int) extends Exp
      case class Add(l: Exp, r: Exp) extends Exp
      case class Bool(b: Boolean) extends Exp
      case class Ifte(cond: Exp, t: Exp, e: Exp) extends Exp

      val thisCompiles: Exp = Add(Lit(3), Bool(true))

    }

    // Same happens in the type class version

    object WithTypeClasses {

      trait Exp[E] {
        def lit(x: Int): E
        def add(l: E, r: E): E
        def bool(b: Boolean): E
        def ifte(cond: E, t: E, e: E): E
      }

      // This compiles but will fail at runtime
      def thisCompiles[E](implicit E: Exp[E]): E =
        E.add(E.lit(3), E.bool(true))

    }

  }

  /**
   * We can use Generalised algebraic data types to avoid this problem.
   */
  object SafeADTs {

    sealed trait Exp[A]
    case class Lit(x: Int) extends Exp[Int]
    case class Add(l: Exp[Int], r: Exp[Int]) extends Exp[Int]
    case class Bool(b: Boolean) extends Exp[Boolean]
    case class Ifte[A](cond: Exp[Boolean], t: Exp[A], e: Exp[A]) extends Exp[A]

    // Now, this doesn't compile
    // val thisDoesntCompile: Exp[Int] = Add(Lit(3), Bool(true))

    def eval[A](exp: Exp[A]): A = exp match {
      case Lit(i) => i
      case Add(l, r) => eval(l) + eval(r)
      case Bool(b) => b
      case Ifte(cond, t, e) =>
        if (eval(cond)) eval(t)
        else eval(e)
    }

    def print[A](e: Exp[A]): String = e match {
      case Lit(i) => i.toString
      case Add(l, r) => s"${print(l)} + ${print(r)}"
      case Bool(b) => b.toString
      case Ifte(cond, t, e) =>
        s"""|if (${print(cond)})
            |  ${print(t)}
            |else
            |  ${print(e)}""".stripMargin
    }

  }

  /**
   * With type classes, we can achieve similar guarantees using type constructor parameters.
   */
  object SafeTypeclasses {

    // The parameter `E[T]` can be understood as the type of an arithmetic expression
    // that will return a value of type `T` when evaluated. 

    trait Expr[E[_]] {
      def lit(i: Int): E[Int]
      def add(e1: E[Int], e2: E[Int]): E[Int]
      def bool(b: Boolean): E[Boolean]
      def ifte[A](cond: E[Boolean], t: E[A], e: E[A]): E[A]
    }

    // This doesn't compile any more:

    // def thisDoesntCompile[E[_]](implicit E: Expr[E]): E[Int] =
    //   E.add(E.lit(3), E.bool(true))

    def thisCompiles[E[_]](implicit E: Expr[E]): E[Int] =
      E.add(E.lit(3), E.lit(4))

    // We need the identity type constructor to evaluate arithmetic expressions.

    implicit object Eval extends Expr[Id] {
      def lit(i: Int): Int = i
      def add(e1: Int, e2: Int): Int = e1 + e2
      def bool(b: Boolean): Boolean = b
      def ifte[A](cond: Boolean, t: A, e: A): A = if (cond) t else e
    }

    // Interpretation
    val evalInt: Int = thisCompiles[Id]

    // We need a constant type constructor for printing expressions.

    type StringF[A] = String

    implicit object Print extends Expr[StringF] {
      def lit(i: Int) = i.toString
      def add(e1: String, e2: String) = s"$e1 + $e2"
      def bool(b: Boolean) = b.toString
      def ifte[A](cond: String, t: String, e: String): String =
        s"""|if ($cond)
            |  $t
            |else
            |  $e""".stripMargin

    }

    // Interpretation
    val printInt: String = thisCompiles[StringF]

  }

}
