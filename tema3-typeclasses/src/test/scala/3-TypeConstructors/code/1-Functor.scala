package org.hablapps.fpinscala.typeclasses
package constructors
package code

import org.scalatest._

class FunctorSpec extends FunSpec with Matchers {

  // 1. Let's tackle the next problem, in our app we have data with different
  // formats:

  case class RawPerson(firstName: String, lastName: String, age: Int, dni: String)
  case class Employee(name: String, age: Int)

  // 2. In determined places we'll have to convert from `RawPerson` to
  // `Employee`, and to that end we have a conversion function

  def toEmployee(p: RawPerson): Employee = p match {
    case RawPerson(first, last, age, _) => Employee(s"$first $last", age)
  }

  // 3. And we could use that function to create other helper methods
  // that work for different data structures as `List` or `Option`

  def listToEmployee(l: List[RawPerson]): List[Employee] =
    l match {
      case h :: t => toEmployee(h) :: listToEmployee(t)
      case Nil => Nil
    }
    // l map toEmployee

  def listToEmployee(o: Option[RawPerson]): Option[Employee] =
    o match {
      case Some(p) => Some(toEmployee(p))
      case None => None
    }
    // o map toEmployee

  // 4. But this is not desirable, can we modularize this functions
  // as we did with `greater` and `collapse`?

  // def toEmployee[F[_]](fa: F[RawPerson]): F[Employee] = ???

  // 5. To achieve it we need to define a type class that allow
  // structures to modify their content

  trait Functor[F[_]] {
    def map[A, B](fa: F[A])(f: A => B): F[B]
  }

  object Functor {
    def apply[F[_]](implicit ev: Functor[F]) = ev

    object syntax {
      def map[F[_], A, B](fa: F[A])(f: A => B)(implicit F: Functor[F]) =
        F.map(fa)(f)
    }
  }

  // 6. Now we have all the ingredients to modularize all functions into
  // only one "vitamined function"

  import Functor.syntax._
  def toEmployee[F[_]: Functor](fa: F[RawPerson]): F[Employee] =
    map(fa)(toEmployee)

  // 7. Let's check out if that worked

  describe("toEmployee") {
    it("should work for Id[X] = X") {
      type Id[X] = X
      implicit val idFunctor = new Functor[Id] {
        def map[A, B](fa: A)(f: A => B): B = f(fa)
      }
      toEmployee[Id](RawPerson("Javier", "Fuentes", 28, "12345678B")) shouldBe
        Employee("Javier Fuentes", 28)
    }

    it("should work for Option") {
      implicit val optionFunctor = new Functor[Option] {
        def map[A, B](fa: Option[A])(f: A => B): Option[B] = fa map f
      }
      toEmployee(Option(RawPerson("Javier", "Fuentes", 28, "12345678B"))) shouldBe
        Option(Employee("Javier Fuentes", 28))
    }

    it("should work for List") {
      implicit val listFunctor = new Functor[List] {
        def map[A, B](fa: List[A])(f: A => B): List[B] = fa map f
      }
      toEmployee( RawPerson("Javier", "Fuentes", 28, "12345678B") ::
                  RawPerson("Morgan", "Freeman", 70, "12345679X") ::
                  Nil) shouldBe
        Employee("Javier Fuentes", 28) ::
        Employee("Morgan Freeman", 70) :: Nil
    }
  }

}
