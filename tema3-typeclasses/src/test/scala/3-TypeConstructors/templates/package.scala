package org.hablapps.fpinscala.typeclasses
package constructors

import org.scalatest._

package object templates extends Templates

class Templates extends FunSpec with Matchers {

  // 1. Let's tackle the next problem, in our app we have data with different
  // formats:

  case class RawPerson(firstName: String, lastName: String, age: Int, dni: String)
  case class Employee(name: String, age: Int)

  // 2. In determined places we'll have to convert from `RawPerson` to
  // `Employee`, and to that end we have a conversion function

  def toEmployee(p: RawPerson): Employee = p match {
    case RawPerson(first, last, age, _) => Employee(s"$first $last", age)
  }

  // 3. And we could use that function to create other helper methods
  // that work for different data structures as `List` or `Option`

  def listToEmployee(l: List[RawPerson]): List[Employee] = ???

  def listToEmployee(o: Option[RawPerson]): Option[Employee] = ???

  // 4. But this is not desirable, can we modularize this functions
  // as we did with `greater` and `collapse`?

  // def toEmployee[F[_]](fa: F[RawPerson]): F[Employee] = ???

  // 5. To achieve it we need to define a type class that allow
  // structures to modify their content

  trait Functor[F[_]]

  // 6. Now we have all the ingredients to modularize all functions into
  // only one "vitamined function"

  def toEmployee[F[_]: Functor](fa: F[RawPerson]): F[Employee] = ???

  // 7. Let's check out if that worked

  describe("toEmployee") {
    ignore("should work for Id[X] = X") {
      type Id[X] = X
      implicit val idFunctor: Functor[Id] = ???
      toEmployee[Id](RawPerson("Javier", "Fuentes", 28, "12345678B")) shouldBe
        (??? : Employee)
    }

    ignore("should work for Option") {
      implicit val optionFunctor: Functor[Option] = ???
      toEmployee(Option(RawPerson("Javier", "Fuentes", 28, "12345678B"))) shouldBe
        (??? : Option[Employee])
    }

    ignore("should work for List") {
      implicit val listFunctor: Functor[List] = ???
      toEmployee( RawPerson("Javier", "Fuentes", 28, "12345678B") ::
                  RawPerson("Morgan", "Freeman", 70, "12345679X") ::
                  Nil) shouldBe
        (??? : List[Employee])
    }
  }

}
