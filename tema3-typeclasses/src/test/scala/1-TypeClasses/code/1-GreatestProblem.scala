package org.hablapps.fpinscala.typeclasses
package towards
package code

import org.scalatest._

abstract class GreatestProblem extends FunSpec with Matchers{

  // We want to implement a function greatest, that given a `List` of items
  // of types Int, String, Char and Potato, it returns the greatest among them,
  // wrapped in an `Option`. If the list is empty, it returns `None`.

  def testGreatestChar(greatest: List[Char] => Option[Char]){

    it("should return none, for empty lists of chars"){
      greatest(List()) shouldBe None
    }

    it("should return the only one, for singleton lists of chars"){
      greatest(List('a')) shouldBe Some('a')
    }

    it("should return the greatest, for lists with several chars"){
      greatest(List('c','a','b')) shouldBe Some('c')
    }
  }

  def testGreatestInt(greatest: List[Int] => Option[Int]){

    it("should return none, for empty lists of ints"){
      greatest(List()) shouldBe None
    }

    it("should return the only one, for singleton lists of ints"){
      greatest(List(1)) shouldBe Some(1)
    }

    it("should return the greatest, for lists with several ints"){
      greatest(List(3,1,4)) shouldBe Some(4)
    }
  }

  def testGreatestString(greatest: List[String] => Option[String]){

    it("should return none, for empty lists of strings"){
      greatest(List()) shouldBe None
    }

    it("should return the only one, for singleton lists of strings"){
      greatest(List("1")) shouldBe Some("1")
    }

    it("should return the greatest string, lexicographically"){
      greatest(List("abc","ca","d")) shouldBe Some("d")
    }
  }

  def testGreatestPotato[A <: Potato](
    potato: ((Int,Int,Int), Int) => A,
    greatest: List[A] => Option[A]){

    it("should return none, for empty lists of potatos"){
      greatest(List()) shouldBe None
    }

    it("should return the only one, for singletons lists of potatos"){
      greatest(List(potato((1,1,1),1))) shouldBe Some(potato((1,1,1),1))
    }

    it("should return the potato of greatest size"){
      greatest(List(potato((1,1,1),0), potato((1,1,1),4),
        potato((1,1,1),2))) shouldBe Some(potato((1,1,1),4))
    }
  }
}