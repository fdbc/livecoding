package org.hablapps.fpinscala.typeclasses
package towards
package code

class GreatestAdapter extends GreatestProblem {

  abstract class Order[A](unwrap: A){
    def compare(other: A): Int

    def >(other: A): Boolean = compare(other) > 0
    def ===(other: A): Boolean = compare(other) == 0
    def <(other: A): Boolean = compare(other) < 0
  }

  // 5. Ok, so far so good, what if we want to sort lists of `Int`s?

  // 6. Oops! we don't have control over the type `Int`, nor we have an interface
  // available, only a class. For those types out of our scope, we can make
  // good use of another OOP pattern... Adapters to the rescue!

  case class IntOrder(unwrap: Int) extends Order[Int](unwrap) {
    def compare(other: Int) = unwrap - other
  }

  // 7. But now our old `greatest` function doesn't fit, we need to modify it again :(

  def greatest[A](l: List[A])(wrap: A => Order[A]): Option[A] =
    l.foldLeft(Option.empty[A]){
      case (Some(y), x) if wrap(x) < y => Option(y)
      case (_, a) => Option(a)
    }

  // For potatos

  case class PotatoOrder(potato: PotatoImpl) extends Order[PotatoImpl](potato){
    def compare(other: PotatoImpl) = potato.size - other.size
  }

  // 8. Finally, we got it! we have a modular function `greatest` that works for
  // every type we can give an order.

  def greatestInt(l: List[Int]): Option[Int] =
    greatest[Int](l)(IntOrder.apply)

  def greatestPotato(l: List[PotatoImpl]): Option[PotatoImpl] =
    greatest(l)(PotatoOrder.apply)

  describe("Greatest problem: adapters solution") {
    testGreatestInt(greatestInt)
    testGreatestPotato[PotatoImpl](PotatoImpl.apply, greatestPotato)
    // ...
  }

}
