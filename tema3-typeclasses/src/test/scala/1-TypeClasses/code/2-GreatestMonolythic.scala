package org.hablapps.fpinscala.typeclasses
package towards
package code

class GreatestMonolythic extends GreatestProblem{

  def greatestChar(l: List[Char]): Option[Char] =
    l.foldLeft(Option.empty[Char]){
      case (Some(y), x) if x < y => Option(y)
      case (_, a) => Option(a)
    }

  def greatestInt(l: List[Int]): Option[Int] =
    l.sortWith(_>_).headOption

  def greatestString(l: List[String]): Option[String] =
    l.sortWith(_>_).headOption

  def greatestPotato(l: List[Potato]): Option[Potato] =
    l.sortWith(_.size > _.size).headOption

  describe("Greatest problem: monolythic solution"){
    testGreatestChar(greatestChar)
    testGreatestInt(greatestInt)
    testGreatestString(greatestString)
    testGreatestPotato(PotatoImpl.apply, greatestPotato)
  }

}