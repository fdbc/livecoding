package org.hablapps.fpinscala.typeclasses
package towards
package code

object GreatestInheritance extends GreatestInheritance
class GreatestInheritance extends GreatestProblem{

  // Of course, we can't give an implementation of this function
  //
  //    def greatest[A](l: List[A]): Option[A]
  //
  // because we know nothing of the type `A`, hence we don't know which one is greater
  // than the other. We can use inheritance and restrict those `A` to extend
  // an interface Order. (Comparable in Java)

  trait Order[A <: Order[A]] {
    def compare(other: A): Int

    def >(other: A): Boolean = compare(other) > 0
    def ===(other: A): Boolean = compare(other) == 0
    def <(other: A): Boolean = compare(other) < 0
  }

  // Now that we have the interface ready to use, let's try again and implement
  // the function greatest.

  def greatest[A <: Order[A]](l: List[A]): Option[A] =
    l match {
      case h :: t =>
        greatest(t) match {
          case s @ Some(max) if h < max => s
          case _ => Some(h)
        }
      case Nil => None
    }

  // Greatest potato

  case class OrderedPotato(color: (Int,Int,Int), size: Int)
  extends Order[OrderedPotato] with Potato{
    def compare(other: OrderedPotato) = size - other.size
  }

  // Test

  describe("Greatest problem: inheritance solution"){
    testGreatestPotato(OrderedPotato.apply, greatest[OrderedPotato])

    // This won't compile!
    // testGreatestChar(greatest[Char])
    // testGreatestInt(greatest[Int])
    // testGreatestString(greatest[String])
  }


}
