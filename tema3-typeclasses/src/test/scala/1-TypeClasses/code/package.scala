package org.hablapps.fpinscala.typeclasses
package towards

package object code{

  trait Potato{
    val color: (Int, Int, Int)
    val size: Int
  }

  case class PotatoImpl(color: (Int,Int,Int), size: Int) extends Potato

}
