package org.hablapps.fpinscala.typeclasses
package towards
package code

class AggregationAdapter extends AggregationProblem{

  abstract class Monoid[A](unwrap: A) {
    def combine(other: A): A
    def zero: A
  }

  case class IntMonoid(unwrap: Int) extends Monoid[Int](unwrap) {
    def combine(other: Int) = unwrap + other
    def zero = 0 // doesn't really depend on unwrap
  }

  def collapse[A <% Monoid[A]](l: List[A]): A = l match {
    case h :: t => h combine collapse(t)
    case Nil => ??? // we can't create an adapter without having an object of type `A`
                    // Hence we can't access the `zero` value
  }
    // 3. Oops! With adapters we can't make use of static values! There's no way
    // we can implement this function with adapters.

  describe("Aggregation problem: monolythic solution"){
    it("can't be implemented"){
      an[Exception] shouldBe thrownBy(
        testAggregationInt(collapse[Int](_)(IntMonoid.apply))
      )
    }
  }

}

