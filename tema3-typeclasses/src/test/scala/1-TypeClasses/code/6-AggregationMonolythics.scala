package org.hablapps.fpinscala.typeclasses
package towards
package code

/**
 * (I) Monolythic versions
 */
object AggregationMonolythic extends AggregationProblem{

  def sum(l: List[Int]): Int =
    l match {
      case Nil => 0
      case x :: r => x + sum(r)
    }

  def smashPotatos(l: List[PotatoImpl]): PotatoImpl =
    l match {
      case Nil => PotatoImpl((0,0,0),0)
      case x :: r => PotatoImpl(x.color, x.size + smashPotatos(r).size)
    }

  describe("Aggregation problem: monolythic solution"){
    testAggregationInt(sum)
    testAggregationPotato(smashPotatos)
  }

}
