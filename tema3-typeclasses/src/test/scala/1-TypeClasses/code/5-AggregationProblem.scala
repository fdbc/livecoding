package org.hablapps.fpinscala.typeclasses
package towards
package code

import org.scalatest._

abstract class AggregationProblem extends FunSpec with Matchers{

  // We want to implement a function greatest, that given a `List` of items
  // of types Int, String, Char and Potato, it returns the greatest among them,
  // wrapped in an `Option`. If the list is empty, it returns `None`.

  def testAggregationInt(sum: List[Int] => Int){

    it("should return zero for the empty list"){
      sum(List()) shouldBe 0
    }

    it("should return the sum, for lists with several ints"){
      sum(List(3,1,4)) shouldBe 8
    }
  }

  def testAggregationString(sum: List[String] => String){

    it("should return the empty string for the empty list"){
      sum(List()) shouldBe ""
    }

    it("should return the concatenation, for lists with several strings"){
      sum(List("a","b","c")) shouldBe "abc"
    }
  }

  def testAggregationPotato(smash: List[PotatoImpl] => PotatoImpl){

    it("should return none, for an empty list of potatos"){
      smash(List()) shouldBe PotatoImpl((0,0,0),0)
    }

    it("should return the smash of all potatoes, with the color of the first one"){
      smash(List(PotatoImpl((1,1,1),0), PotatoImpl((2,2,2),4),
        PotatoImpl((3,3,3),2))) shouldBe PotatoImpl((1,1,1),6)
    }
  }
}
