package org.hablapps.fpinscala.typeclasses
package towards
package exercises
package solution

import code.PotatoImpl

class GreatestWithClass extends code.GreatestProblem{

  /**
   * API 
   */

  trait Order[A]{
    def compare(one: A, unother: A): Int

    def >(one: A, unother: A): Boolean = compare(one, unother) > 0
    def ===(one: A, unother: A): Boolean = compare(one, unother) == 0
    def <(one: A, unother: A): Boolean = compare(one, unother) < 0
  }

  /** 
   * FUNCTION OVER API
   */

  def greatest[A](l: List[A])(order: Order[A]): Option[A] =
    l.foldLeft(Option.empty[A]){
      case (Some(y), x) if order.<(x , y) => Option(y)
      case (_, a) => Option(a)
    }

  /**
   * API INSTANCES
   */

  object IntOrder extends Order[Int] {
    def compare(one: Int, unother: Int) = one - unother
  }

  object PotatoOrder extends Order[PotatoImpl]{
    def compare(one: PotatoImpl, unother: PotatoImpl) = one.size - unother.size
  }

  /**
   * MODULAR FUNCTIONS
   */

  def greatestInt(l: List[Int]): Option[Int] =
    greatest[Int](l)(IntOrder)

  def greatestPotato(l: List[PotatoImpl]): Option[PotatoImpl] =
    greatest(l)(PotatoOrder)

  /**
   * TESTING
   */

  describe("Greatest problem: adapters solution") {
    testGreatestInt(greatestInt)
    testGreatestPotato[PotatoImpl](PotatoImpl.apply, greatestPotato)
    // ...
  }

}