package org.hablapps.fpinscala.typeclasses
package towards
package exercises

import code.PotatoImpl

class GreatestWithClass extends code.GreatestProblem{

  /**
   * API 
   */

  // abstract class Order[A](unwrap: A){
  //   def compare(other: A): Int

  //   def >(other: A): Boolean = compare(other) > 0
  //   def ===(other: A): Boolean = compare(other) == 0
  //   def <(other: A): Boolean = compare(other) < 0
  // }

  /** 
   * FUNCTION OVER API
   */

  // def greatest[A](l: List[A])(wrap: A => Order[A]): Option[A] =
  //   l.foldLeft(Option.empty[A]){
  //     case (Some(y), x) if wrap(x) < y => Option(x)
  //     case (_, a) => Option(a)
  //   }

  /**
   * API INSTANCES
   */

  // case class IntOrder(unwrap: Int) extends Order[Int](unwrap) {
  //   def compare(other: Int) = unwrap - other
  // }

  // case class PotatoOrder(potato: PotatoImpl) extends Order[PotatoImpl](potato){
  //   def compare(other: PotatoImpl) = potato.size - other.size
  // }

  /**
   * MODULAR FUNCTIONS
   */

  def greatestInt(l: List[Int]): Option[Int] = ???
  //   greatest[Int](l)(IntOrder.apply)

  def greatestPotato(l: List[PotatoImpl]): Option[PotatoImpl] = ???
  //   greatest(l)(PotatoOrder.apply)

  /**
   * TESTING
   */

  ignore("Greatest problem: adapters solution") {
    testGreatestInt(greatestInt)
    testGreatestPotato[PotatoImpl](PotatoImpl.apply, greatestPotato)
    // ...
  }

}