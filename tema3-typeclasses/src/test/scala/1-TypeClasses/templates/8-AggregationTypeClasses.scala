package org.hablapps.fpinscala.typeclasses
package towards
package templates

class AggregationMonoid extends AggregationProblem{

  // API

  trait Monoid[A]
  // abstract class Monoid[A](unwrap: A) {
  //   def combine(other: A): A
  //   def zero: A
  // }

  // FUNCTION OVER API

  def collapse[A](l: List[A]): A = ???

  // API INSTANCE

  lazy val intMonoid: Monoid[Int] = ???

  object potatoMonoid extends Monoid[PotatoImpl]

  // TEST

  describe("Aggregation problem: with type classes"){
    // testAggregationInt(collapse[Int](_)(intMonoid))
    // testAggregationPotato(collapse[PotatoImpl](_)(potatoMonoid))
  }
}
