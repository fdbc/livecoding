package org.hablapps.fpinscala.typeclasses
package towards
package templates

class GreatestMonolythic extends GreatestProblem{

  def greatestChar(l: List[Char]): Option[Char] =
    ???

  def greatestInt(l: List[Int]): Option[Int] =
    ???

  def greatestString(l: List[String]): Option[String] =
    ???

  def greatestPotato(l: List[Potato]): Option[Potato] =
    ???

  ignore("Greatest problem: monolythic solution"){
    testGreatestChar(greatestChar)
    testGreatestInt(greatestInt)
    testGreatestString(greatestString)
    testGreatestPotato(PotatoImpl.apply, greatestPotato)
  }

}