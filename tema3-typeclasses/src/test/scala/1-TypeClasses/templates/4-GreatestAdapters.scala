package org.hablapps.fpinscala.typeclasses
package towards
package templates

class GreatestAdapter extends GreatestProblem {

  // API

  abstract class Order

  // FUNCTION OVER API

  def greatest[A](l: List[A]): Option[A] = ???
    // l.foldLeft(Option.empty[Char]){
    //   case (Some(y), x) if x < y => Option(y)
    //   case (_, a) => Option(a)
    // }
  
  // API INSTANCES

  
  // MODULAR IMPLEMENTATIONS

  def greatestInt(l: List[Int]): Option[Int] = ???

  def greatestPotato(l: List[PotatoImpl]): Option[PotatoImpl] = ???

  // TEST

  ignore("Greatest problem: adapters solution") {
    testGreatestInt(greatestInt)
    testGreatestPotato[PotatoImpl](PotatoImpl.apply, greatestPotato)
    // ...
  }

}
