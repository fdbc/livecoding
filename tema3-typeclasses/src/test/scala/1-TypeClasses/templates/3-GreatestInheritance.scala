package org.hablapps.fpinscala.typeclasses
package towards
package templates

object GreatestInheritance extends GreatestInheritance
class GreatestInheritance extends GreatestProblem{

  // API

  trait Order

  // FUNCTION OVER API

  def greatest[A](l: List[A]): Option[A] = ???
    // l.foldLeft(Option.empty[Char]){
    //   case (Some(y), x) if x < y => Option(x)
    //   case (_, a) => Option(a)
    // }


  // API INSTANCES

  case class OrderedPotato()

  // TEST

  ignore("Greatest problem: inheritance solution"){
    // testGreatestPotato(OrderedPotato.apply, greatest[OrderedPotato])

    // This won't compile!
    // testGreatestChar(greatest[Char])
    // testGreatestInt(greatest[Int])
    // testGreatestString(greatest[String])
  }


}
