package org.hablapps.fpinscala.typeclasses
package towards
package templates

class AggregationAdapter extends AggregationProblem{

  // API

  abstract class Monoid[A](unwrap: A) {
    def combine(other: A): A
    def zero: A
  }

  // FUNCTION OVER API

  def collapse[A <% Monoid[A]](l: List[A]): A = ???
  
  // API INSTANCE

  case class IntMonoid(unwrap: Int) extends Monoid[Int](unwrap) {
    def combine(other: Int) = ???
    def zero = ???
  }

  // TEST

  ignore("Aggregation problem: monolythic solution"){
    it("can't be implemented"){
      an[Exception] shouldBe thrownBy(
        testAggregationInt(collapse[Int](_)(IntMonoid.apply))
      )
    }
  }
}

