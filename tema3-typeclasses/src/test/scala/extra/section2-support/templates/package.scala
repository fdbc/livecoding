package org.hablapps.fpinscala.typeclasses
package support

import org.scalatest._

package object templates extends Templates

class Templates extends FunSpec with Matchers {

  import intro.code.TypeClasses.Monoid

  trait Monoid[T]{
    val zero: T
    def add(t1: T,t2: T): T
  }

  object Monoid{
  }

  // IMPLICITS
  object ImplicitParameters{

    // Function over API
    def collapse[A](t: List[A])(monoid: Monoid[A]): A =
      t match {
        case Nil => monoid.zero
        case h::t =>
          monoid.add(collapse(t)(monoid),h)
      }

    // Instances
    val intMonoid: Monoid[Int] = new Monoid[Int]{
      val zero = 0
      def add(t1: Int, t2: Int): Int = t1 + t2
    }

    // Composition
    def sum(t: List[Int]): Int = ???

  }

  describe("La version con implicitos") {
    import ImplicitParameters._

    ignore("debería funcionar correctamente") {
      sum(List(1, 2, 3, 4)) shouldBe 10
    }
  }
  
  // CONTEXT BOUNDS
  object ContextBounds{
    
    // Function over API

    def collapse[A](t: List[A]): A = ???


  }

  describe("La version con context bounds") {
    import ContextBounds._

    ignore("debería funcionar correctamente") {
      // Composition
      collapse(List(1, 2, 3, 4)) shouldBe 10
    }
  }

  // SYNTAX
  object SyntacticSugar{

    object MonoidSyntax {

      // Binary ops

      // Other ops

    }

    // Function over API

    import MonoidSyntax._
    def collapseSTX[A](t: List[A]): A = ???
    
  }

  describe("La version con syntax") {
    import SyntacticSugar._

    ignore("debería funcionar correctamente") {
      // Composition
      collapseSTX(List(1, 2, 3, 4)) shouldBe 10
    }
  }
}
