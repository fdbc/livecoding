package org.hablapps.fpinscala.typeclasses
package oo
package code

object AdaptersProblems{

  /**
   * (II) Recurrrent pattern
   */
  object RecurrentPattern{

    /**
     * Adapter
     */
    trait Monoid[A]{
      val unwrap: A
      def combine(other: A): A
      def zero: A
    }

    /**
     * Can't implement a fully working generic version!
     */
    def collapse[A](l: List[A])(monoid: A => Monoid[A]): A = l match {
      case Nil => ??? // can't access `zero`!
      case x :: r => monoid(x).combine(collapse(r)(monoid))
    }

  }

  /**
   * (III) Modularised versions
   */
  object ModularPrograms{
    import RecurrentPattern._

    // Adapter instances for standard types

    implicit class IntMonoid(val unwrap: Int) extends Monoid[Int] {
      def combine(i2: Int): Int = unwrap + i2
      // Doesn't really use `unwrap`, this value is "static"
      def zero: Int = 0
    }

    def sumInt(l: List[Int]): Int =
      collapse(l)(IntMonoid(_))

  }


}


