package org.hablapps.fpinscala.typeclasses
package oo
package code

/**
 * (I) Monolythic versions
 */
object MonolythicPrograms{

  /* For predefined types */

  def greatestChar(l: List[Char]): Option[Char] =
    l.foldLeft(Option.empty[Char]){
      (acc, x) => acc.fold(Option(x)){
        y => if (x < y) Option(y) else Option(x)
      }
    }

  def greatestInt(l: List[Int]): Option[Int] =
    l.sortWith(_>_).headOption

  def greatestString(l: List[String]): Option[String] =
    l.sortWith(_>_).headOption

  /* For our own types */

  import intro.code.MonolythicPrograms.Potato

  def greatestPotato(l: List[Potato]): Option[Potato] =
    l.sortWith(_.size > _.size).headOption
}
