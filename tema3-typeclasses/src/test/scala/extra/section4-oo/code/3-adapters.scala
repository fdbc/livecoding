package org.hablapps.fpinscala.typeclasses
package oo
package code

object Adapters{

  /**
   * (II) Recurrrent pattern
   */
  object RecurrentPattern{

    /**
     * Adapter
     */
    trait Order[A]{
      val unwrap: A
      def compare(other: A): Int
      // derived
      def greaterThan(t2: A): Boolean = compare(t2) > 0
      def equalThan(t2: A): Boolean = compare(t2) == 0
      def lowerThan(t2: A): Boolean = compare(t2) < 0
    }

    /**
     * Generic implementation using the adapter
     */
    // def greatest[A <% Order[A]](l: List[A]): Option[A] =
    def greatest[A](l: List[A])(ord: A => Order[A]): Option[A] =
      l.foldLeft(None: Option[A]){
        (acc, x) => acc.fold(Option(x)){
          y => if (ord(x).lowerThan(y)) Option(y) else Option(x)
        }
      }

  }

  /**
   * (III) Modularised versions
   */
  object ModularPrograms{
    import RecurrentPattern._

    // Adapter instances for standard types

    implicit class IntOrder(val unwrap: Int) extends Order[Int] {
      def compare(i2: Int): Int = unwrap - i2
    }

    implicit class CharOrder(val unwrap: Char) extends Order[Char] {
      def compare(c2: Char) =
        if (unwrap > c2) 1
        else if (unwrap == c2) 0
        else -1
    }

    implicit class StringOrder(val unwrap: String) extends Order[String] {
      def compare(s2: String) =
        if (unwrap > s2) 1
        else if (unwrap == s2) 0
        else -1
    }

    // Adapter instances for our own types
    import intro.code.MonolythicPrograms.Potato

    implicit class PotatoOrder(val unwrap: Potato) extends Order[Potato]{
      def compare(other: Potato): Int =
        unwrap.size - other.size
    }

    // Modularised versions

    def greatestChar(l: List[Char]): Option[Char] =
      greatest(l)(CharOrder(_))

    def greatestInt(l: List[Int]): Option[Int] =
      greatest(l)(IntOrder(_))

    def greatestString(l: List[String]): Option[String] =
      greatest(l)(StringOrder(_))

    def greatestPotato(l: List[Potato]): Option[Potato] =
      greatest(l)(PotatoOrder(_))
  }

}


