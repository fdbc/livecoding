package org.hablapps.fpinscala.typeclasses
package oo
package templates

object AdaptersProblems{

  /**
   * (II) Recurrrent pattern
   */
  object RecurrentPattern{

    /**
     * Adapter
     */

    /**
     * Can't implement a fully working generic version!
     */

  }

  /**
   * (III) Modularised versions
   */
  object ModularPrograms{
    import RecurrentPattern._

    // Adapter instances for standard types


  }


}


