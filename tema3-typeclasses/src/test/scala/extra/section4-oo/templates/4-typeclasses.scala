package org.hablapps.fpinscala.typeclasses
package oo
package templates

import org.scalatest._

class TypeClasses extends FunSpec with Matchers{

  /**
   * (II) Recurrent patterns
   */

  import support.code.Implicits.Monoid
  import support.code.Implicits.ImplicitParameters.collapse

    
  /**
   * (III) Modularisation
   */
  
  import intro.code.MonolythicPrograms.Potato

  implicit object potatoMonoid extends Monoid[Potato]{
    val zero = ???
    def add(t1: Potato, t2: Potato): Potato = ???
  }

  def smashPotatos(l: List[Potato]): Potato = ???
}