package org.hablapps.fpinscala.typeclasses
package oo
package templates

object Inheritance{

  /**
   * (II) Recurrrent pattern: failed attempt with inheritance
   */
  object RecurrentPattern{

    /**
     * Base class
     */
    trait Order

    /**
     * Generic implementation
     */
    
  }

  /**
   * (III) Modularised versions
   */
  object ModularPrograms{
    import RecurrentPattern._

    // Can't redefine standard types
    // e.g. `class Int extends Order ...`

    // We can only do it with our own classes

    
    // Modularised versions

    
  }
}