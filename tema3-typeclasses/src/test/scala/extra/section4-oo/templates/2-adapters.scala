package org.hablapps.fpinscala.typeclasses
package oo
package templates

object Adapters{

  /**
   * (II) Recurrrent pattern
   */
  object RecurrentPattern{

    /**
     * Adapter
     */
    
    /**
     * Generic implementation using the adapter
     */
    
  }

  /**
   * (III) Modularised versions
   */
  object ModularPrograms{
    import RecurrentPattern._

    // Adapter instances for standard types

    
    // Adapter instances for our own types
    
    // Modularised versions


  }

}


