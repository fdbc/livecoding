package org.hablapps.fpinscala.typeclasses
package constructors
package code

import org.scalatest._

object HigherKindsTypeClasses extends HigherKindsTypeClasses

class HigherKindsTypeClasses extends FunSpec with Matchers {

  // We can define a `map` function for different data structures
  // e.g., Option or List. In both cases, the intention is that the 
  // map function changes the content of the data structures, without
  // any rearrangement or alteration of the structure. 

  // `List[A]`
  def map[A, B](l: List[A])(f: A => B): List[B] =
    l.foldRight(List.empty[B])(f(_) :: _)

  describe("Map for lists") {
    it("should work") {
      map(List(1, 2, 3, 4))(_ * 2) shouldBe List(2, 4, 6, 8)
    }
  }

  // `Option[A]`
  def map[A, B](o: Option[A])(f: A => B): Option[B] =
    o.fold(Option.empty[B])(f andThen Option.apply)

  describe("Map for options") {
    it("should work") {
      map(Option(3))(_ * 2) shouldBe Option(6)
    }
  }

  // We can use these functions to implement the following 
  // `duplicate` functions

  def duplicate[A](l: List[A]): List[(A,A)] =
    map(l)(a => (a, a))

  describe("Duplicate for lists") {
    it("should work") {
      duplicate(List(1, 2, 3, 4)) shouldBe List((1, 1), (2, 2), (3, 3), (4, 4))
    }
  }

  def duplicate[A](o: Option[A]): Option[(A,A)] =
    map(o)(a => (a, a))

  describe("Duplicate for options") {
    it("should work") {
      duplicate(Option(3)) shouldBe Option((3, 3))
    }
  }

  // The implementation in both cases is essentially the same. In order to remove
  // this redundancy we package the `map` function in a new type class, namely
  // the `Functor` type class.

  trait Functor[F[_]]{
    // 1. Abstract
    def map[A, B](fa: F[A])(f: A => B): F[B]

    // 2. Concrete
    def lift[A, B](f: A => B): F[A] => F[B] = map(_)(f)
    def as[A, B](fa: F[A], b: B): F[B] = map(fa)(_ => b)
  }

  object Functor extends FunctorInstances
    with FunctorSyntax
    with FunctorLaws

  // 3. Instances
  trait FunctorInstances {
    def apply[F[_]](implicit ev: Functor[F]) = ev

    implicit val optionFunctor = new Functor[Option]{
      def map[A, B](fa: Option[A])(f: A => B): Option[B] =
        fa map f
    }

    implicit val listFunctor = new Functor[List]{
      def map[A, B](fa: List[A])(f: A => B): List[B] =
        fa map f
    }
  }

  // 4. Syntax
  trait FunctorSyntax {
    object syntax {
      implicit class FunctorOps[F[_], A](fa: F[A])(implicit ev: Functor[F]) {
        def map[B](f: A => B): F[B] = ev.map(fa)(f)
      }
    }
  }

  // 5. Laws
  trait FunctorLaws {
    import Functor.syntax._

    trait Laws[F[_]] {
      implicit val functor: Functor[F]

      def identity[A](fa: F[A]): Boolean =
        (fa map (x => x)) == fa
      def composite[A, B, C](fa: F[A], f1: A => B, f2: B => C): Boolean =
        (fa map f1 map f2) == (fa map (f1 andThen f2))
    }

    def laws[F[_]](implicit ev: Functor[F]) = new Laws[F] { implicit val functor = ev }
  }


  // Check laws

  describe("Check functor laws"){
    it("Identity law"){
      Functor.laws[Option].identity(Option(3)) shouldBe true
    }

    it("Composition law"){
      Functor.laws[Option].composite(Option(3),
        (i: Int) => i.toString,(s: String) => s.length) shouldBe true
    }
  }

  // Now we can implement `duplicate` in a generic way.
  import Functor.syntax._
  def duplicate[F[_]: Functor, A](o: F[A]): F[(A, A)] =
    o.map(a => (a,a))

  describe("Duplicate for F[_]: Functor") {
    it("should work for F[_] = Option") {
      duplicate[Option, Int](Option(3)) shouldBe Option((3, 3))
    }
    it("should work for F[_] = List") {
      duplicate[List, Int](List(1, 2, 3, 4)) shouldBe List((1, 1), (2, 2), (3, 3), (4, 4))
    }
  }

}