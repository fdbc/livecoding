package org.hablapps.fpinscala.typeclasses
package intro
package code

import org.scalatest._

trait IntroSpec { self: FunSpec with Matchers =>
  import MonolythicPrograms.Potato

  def addIntTest(sum: List[Int] => Int) =
    it("should work properly with ints"){
      sum(List(1, 2, 3, 4)) shouldBe 10
    }

  def concatTest(concat: List[String] => String) =
    it("should work properly with String"){
      concat(List("hola", "a", "todo", "el", "mundo")) shouldBe "holaatodoelmundo"
    }

  def smashTest(smash: List[Potato] => Potato) =
    it("should work properly with Potato"){
      smash(List(Potato((1,1,1),1),Potato((2,2,2),2))) shouldBe Potato((1,1,1),3)
    }

}