package org.hablapps.fpinscala.typeclasses
package intro
package code

import org.scalatest._

/**
  We can group together within a new abstraction the extra information
  required from a give type parameter. In that way, we can save the extra
  effort of passing all components individually.
*/

object TypeClasses extends TypeClasses

class TypeClasses extends FunSpec with Matchers with IntroSpec {

  // For our example, we can group the operations in a new abstraction
  // named `Monoid`. This type class represents the class of types that
  // can be combined.

  trait Monoid[T]{
    val zero: T
    def add(t1: T,t2: T): T
  }

  // Now, we can pass the `zero` value and `add` function at the same time.
  def collapse[A](t: List[A])(monoid: Monoid[A]): A =
    t match {
      case Nil => monoid.zero
      case h :: t =>
        monoid.add(h, collapse(t)(monoid))
    }

  // Instances of the `Monoid` type class, i.e. evidence Integers and String
  // belong to this type class.

  val intMonoid: Monoid[Int] = new Monoid[Int]{
    val zero = 0
    def add(t1: Int, t2: Int): Int = t1 + t2
  }

  object stringMonoid extends Monoid[String]{
    val zero: String = ""
    def add(t1: String, t2: String): String = t1 + t2
  }

  def sum(l: List[Int]): Int =
    collapse[Int](l)(intMonoid)

  def concat(l: List[String]): String =
    collapse[String](l)(stringMonoid)

  describe("La version con typeclass") {
    addIntTest(sum)
    concatTest(concat)
  }

}
