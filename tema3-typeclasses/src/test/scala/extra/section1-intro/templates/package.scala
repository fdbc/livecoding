package org.hablapps.fpinscala.typeclasses
package intro

import org.scalatest._

package object templates extends Templates

class Templates extends FunSpec with Matchers {

  // Funciones monolíticas
  def sum(l: List[Int]): Int =
    l match {
      case Nil => 0
      case x :: r => x + sum(r)
    }

  def concat(l: List[String]): String =
    l match {
      case Nil => ""
      case x :: r => x + concat(r)
    }

  def hofsTest(sum: List[Int] => Int, concat: List[String] => String) = {
    it("should work properly"){
      // sum(List(1, 2, 3, 4)) shouldBe ???
      // concat(List("hola", "a", "todo", "el", "mundo")) shouldBe ???
    }
  }

  describe("Monolythic functions") {
    hofsTest(sum, concat)
  }

  // Abstraction
  // ???

  // Composition
  def sumBis(l: List[Int]): Int = ???

  def concatBis(l: List[String]): String = ???

  ignore("// Modularised function collapse") {
    hofsTest(sumBis, concatBis)

    it("serves for other purposes as well") {
      // collapse(List(1, 2, 3, 4))(???, ???) shouldBe ???
    }
  }

  /**
  We can group together within a new abstraction the extra information 
  required from a give type parameter. In that way, we can save the extra
  effort of passing all components individually.
  */
  trait Monoid

  // Now, we can pass the `zero` value and `add` function at the same time.
  // def collapse

  // Instances of the `Monoid` type class, i.e. evidence Integers and String
  // belong to this type class.
  // val intMonoid
  // object stringMonoid

  // Modularised versions
  
  def sumTC(l: List[Int]): Int = ???

  def concatTC(l: List[String]): String = ???

  ignore("La version con typeclass") {
    hofsTest(sumTC, concatTC)
  }

}
