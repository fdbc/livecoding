package org.hablapps.fpinscala
package languages
package solution

object Homework1{
  import MonadicPrograms.Monad
  import FS.Syntax._, Monad.Syntax._

  /**
   * Part I.
   *
   * Implement a program that moves a file from one place to another.
   */
  import Exercise2Program1.Declarative.copy

  def move[F[_]: FS: Monad](orig: String, dest: String): F[Unit] =
    for {
      _ <- copy(orig, dest)
      _ <- deleteFile(orig)
    } yield ()

  /**
   * Part II.
   *
   * Implement a program that merges together the content of two files.
   */
  def merge2[F[_]: FS: Monad](orig1: String, orig2: String, dest: String): F[Unit] =
    for {
      contents1 <- readFile(orig1)
      contents2 <- readFile(orig2)
      _ <- writeFile(dest)(contents1 + contents2)
    } yield ()

  /**
   * Part III.
   *
   * Implement a program that does the same as the above, but also returns
   * the whole length of the destination file.
   */
  def merge2bis[F[_]: FS: Monad](orig1: String, orig2: String, dest: String): F[Int] =
    for {
      contents1 <- readFile(orig1)
      contents2 <- readFile(orig2)
      wholeContent = contents1 + contents2
      _ <- writeFile(dest)(wholeContent)
    } yield wholeContent.length
    // Reusing `merge2`:
    // for {
    //   _ <- merge2(orig1, orig2, dest)
    //   contents <- readFile(dest)
    // } yield contents.length

  /**
   * Part IV.
   *
   * Implement a program similar to `merge2`, but fo N files.
   */
  def mergeN[F[_]: FS: Monad](origs: Seq[String], dest: String): F[Unit] = {
    val wholeContent: F[String] = origs.foldLeft(returns(""))(
      (acc, orig) =>
        for {
          a <- acc
          contents <- readFile(orig)
        } yield a + contents)
    wholeContent flatMap writeFile[F](dest)
  }

}