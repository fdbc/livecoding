package org.hablapps.fpinscala
package languages

import scala.io.StdIn.readLine
import org.scalatest._

object MultipleAPIs extends MultipleAPIs

class MultipleAPIs extends FlatSpec with Matchers{

  // Log API

  sealed abstract class Level
  case object WARNING extends Level
  case object DEBUG extends Level
  case object INFO extends Level
  case object TRACE extends Level

  trait Log[P[_]]{
    def log(level: Level, msg: String): P[Unit]
    def trace(msg: String): P[Unit] = log(TRACE,msg)
  }

  object Log{

    object Syntax{
      def log[P[_]](level: Level, msg: String)(implicit Log: Log[P]) =
        Log.log(level,msg)
      def trace[P[_]](msg: String)(implicit Log: Log[P]) =
        Log.trace(msg)
    }

    implicit object ConsoleLog extends Log[Id]{
      def log(level: Level, msg: String) =
        println(s"$level: $msg")
    }

  }

  // API program

  import MonadicPrograms.Monad, MonadicPrograms.IO
  import IO.Syntax._, Log.Syntax._, Monad.Syntax._

  def echo[P[_]: IO: Log: Monad](): P[String] = for {
    msg <- read()
    _ <- trace(s"read '$msg'")
    _ <- write(msg)
    _ <- trace(s"written '$msg'")
  } yield msg


  // Composition

  def consoleEcho(): Unit =
    echo[Id]()

}