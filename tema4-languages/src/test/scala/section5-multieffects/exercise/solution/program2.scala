package org.hablapps.fpinscala.languages
package solution

object Exercise2Program2{

  // Conventional version
  object Conventional{
    import ConventionalFS.FS, ConventionalApproach.Modular.IO

    def copy(orig: String, dest: String)(io: IO, fs: FS): Unit = {
      io.write(s"copying $orig into $dest")
      val content: String = fs.readFile(orig)
      fs.writeFile(dest)(content)
    }
  }

  // Declarative version
  object Declarative{
    import MonadicPrograms.IO, MonadicPrograms.Monad
    import FS.Syntax._, IO.Syntax._, Monad.Syntax._

    def copy[F[_]: FS: IO: Monad](orig: String, dest: String): F[Unit] =
      for {
        _ <- write(s"copying $orig into $dest")
        content <- readFile(orig)
        _ <- writeFile(dest)(content)
      } yield ()
  }
}
