package org.hablapps.fpinscala.languages
package solution

object Exercise2Program1{

  // Conventional version
  object Conventional{
    import ConventionalFS.FS

    def copy(orig: String, dest: String)(fs: FS): Unit = {
      val content: String = fs.readFile(orig)
      fs.writeFile(dest)(content)
    }
  }

  // Declarative version
  object Declarative{
    import MonadicPrograms.Monad
    import FS.Syntax._, Monad.Syntax._

    def copy[F[_]: FS: Monad](orig: String, dest: String): F[Unit] =
      for {
        content <- readFile(orig)
        _ <- writeFile(dest)(content)
      } yield ()
  }
}
