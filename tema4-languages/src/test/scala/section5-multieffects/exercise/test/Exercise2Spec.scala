package org.hablapps.fpinscala.languages
package test

import org.scalatest._
import Homework1._
import MonadUtils._

class Exercise2Spec extends FlatSpec with Matchers {
  import Exercise2Program1.Declarative.{copy => copyWithoutIO}
  import Exercise2Program2.Declarative.{copy => copyWithIO}

  "Program 1: copy (without IO)" should "create a new file with the same content" in {
    val fs = FSS(Map(
      "file1" -> "file1 contents"))

    copyWithoutIO[FSS.FSState]("file1", "file2").exec(fs) shouldBe
      FSS(Map(
        "file1" -> "file1 contents",
        "file2" -> "file1 contents"))
  }

  it should "copy the content to an existing file" in {
    val fs = new FSS(Map(
      "file1" -> "file1 contents",
      "file2" -> "file2 contents"))

    copyWithoutIO[FSS.FSState]("file1", "file2").exec(fs) shouldBe
      FSS(Map(
        "file1" -> "file1 contents",
        "file2" -> "file1 contents"))
  }

  import TerminalState.TState

  "Program 2: copy (with IO)" should "create a new file with the same content" in {
    val ts = TerminalState(
      IOState(Nil,Nil),
      FSS(Map("file1" -> "file1 contents")))

    copyWithIO[TState]("file1", "file2").exec(ts) shouldBe
      TerminalState(
        IOState(Nil,List("copying file1 into file2")),
        FSS(Map(
          "file1" -> "file1 contents",
          "file2" -> "file1 contents")))
  }

  it should "copy the content to an existing file" in {
    val ts = TerminalState(
      IOState(Nil,Nil),
      FSS(Map(
        "file1" -> "file1 contents",
        "file2" -> "file2 contents")))

    copyWithIO[TState]("file1", "file2").exec(ts) shouldBe
      TerminalState(
        IOState(Nil,List("copying file1 into file2")),
        FSS(Map(
          "file1" -> "file1 contents",
          "file2" -> "file1 contents")))
  }

}
