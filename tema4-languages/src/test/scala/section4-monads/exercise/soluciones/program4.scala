package org.hablapps.fpinscala
package languages
package solution

/**
 * Same as in Program3 but now the program echoes user input.
 *
 * For instance:
 *
 * scala> readUntilExit2
 *   <<type "hi" and enter>>
 *   hi
 *   <<type "bye" and enter>>
 *   bye
 *   <<type "exit" and enter>>
 */
object Program4{

  // Impure version
  import ConventionalApproach.Modular.IO

  trait Conventional{ IO: IO with Program1.Conventional =>
    import IO._

    def readUntilExit: Unit = {
      val msg = read
      write(msg)
      if (msg == "exit") ()
      else readUntilExit
    }
  }

  // Declarative version
  object Declarative{
    import MonadicPrograms.{Monad, IO}, IO.Syntax._, Monad.Syntax._

    def readUntilExit[F[_]: IO: Monad]: F[Unit] =
      for {
        msg <- read
        _ <- write(msg)
        _ <-  if (msg == "exit") returns(())
              else readUntilExit[F]
      } yield ()
  }

}
