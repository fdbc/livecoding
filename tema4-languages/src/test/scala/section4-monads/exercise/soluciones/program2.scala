package org.hablapps.fpinscala
package languages
package solution

/**
 * Same as in `Program1` but now the program asks politely
 * for a number to the user.
 *
 * For instance:
 *
 *   scala> runWriteANumber2
 *   Please, type a number:
 *   << type 8 >>
 *   8 is even
 */
object Program2{

  // Impure version
  import ConventionalApproach.Modular.IO

  trait Conventional{ IO: IO with Program1.Conventional =>
    import IO._

    def writeANumberBis: Unit = {
      write("Introduce un número por favor:")
      writeANumber
    }

  }

  // Declarative version
  object Declarative{
    import MonadicPrograms.{Monad, IO}, IO.Syntax._, Monad.Syntax._
    import Program1.Declarative.writeANumber

    def writeANumberBis[F[_]: IO: Monad]: F[Unit] =
      for {
        _ <- write("Introduce un número por favor:")
        _ <- writeANumber[F]
      } yield ()
  }

}
