package org.hablapps.fpinscala
package languages
package test

import org.scalatest._

class Exercise1Spec extends FunSpec with Matchers {

  import IOState._

  // replace "ignore" with "describe" to test the program
  ignore("WriteANumber"){
    it("should leer el número 3 y decir que es impar"){
      val ios = IOState("3" :: Nil, Nil)
      Program1.Declarative.writeANumber[IOTrans].exec(ios) shouldBe IOState(Nil, "3 es un número impar" :: Nil)
    }

    it("should leer el número 4 y decir que es par"){
      val ios = IOState("4" :: Nil, Nil)
      Program1.Declarative.writeANumber[IOTrans].exec(ios) shouldBe IOState(Nil, "4 es un número par" :: Nil)
    }
  }

  ignore("WriteANumber2"){
    it("should leer el número 3 y decir que es impar"){
      val ios = IOState("3" :: Nil, Nil)
      Program2.Declarative.writeANumberBis[IOTrans].exec(ios) shouldBe
        IOState(Nil, "3 es un número impar" :: "Introduce un número por favor:" :: Nil)
    }

    it("should leer el número 4 y decir que es par"){
      val ios = IOState("4" :: Nil, Nil)
      Program2.Declarative.writeANumberBis[IOTrans].exec(ios) shouldBe
        IOState(Nil, "4 es un número par" :: "Introduce un número por favor:" :: Nil)
    }
  }

  ignore("ReadUntilExit"){
    it("should leer cadenas de texto hasta encontrar `exit`"){
      val ios = IOState("uno" :: "dos" :: "tres" :: "exit" :: Nil, Nil)
      Program3.Declarative.readUntilExit[IOTrans].exec(ios) shouldBe
        IOState(Nil, Nil)
    }

    it("should parar de leer cuando encuentra un `exit`"){
      val ios = IOState("uno" :: "dos" :: "tres" :: "exit" :: "otro" :: Nil, Nil)
      Program3.Declarative.readUntilExit[IOTrans].exec(ios) shouldBe
        IOState("otro" :: Nil, Nil)
    }
  }

  ignore("ReadUntilExit2"){
    it("should leer cadenas de texto hasta encontrar `exit`"){
      val ios = IOState("uno" :: "dos" :: "tres" :: "exit" :: Nil, Nil)
      Program4.Declarative.readUntilExit[IOTrans].exec(ios) shouldBe
        IOState(Nil, "exit" :: "tres" :: "dos" :: "uno" :: Nil)
    }

    it("should parar de leer cuando encuentra un `exit`"){
      val ios = IOState("uno" :: "dos" :: "tres" :: "exit" :: "otro" :: Nil, Nil)
      Program4.Declarative.readUntilExit[IOTrans].exec(ios) shouldBe
        IOState("otro" :: Nil, "exit" :: "tres" :: "dos" :: "uno" :: Nil)
    }
  }

}
