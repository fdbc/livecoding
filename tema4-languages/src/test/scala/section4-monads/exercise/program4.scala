package org.hablapps.fpinscala
package languages

/**
 * Same as in Program3 but now the program echoes user input.
 *
 * For instance:
 *
 * scala> readUntilExit2
 *   <<type "hi" and enter>>
 *   hi
 *   <<type "bye" and enter>>
 *   bye
 *   <<type "exit" and enter>>
 *
 * Use the following alias to test this exercise:
 *   `test-languages-exercise1`
 */
object Program4{

  // Impure version

  object Conventional{
    import ConventionalApproach.Modular.IO

    def readUntilExit()(io: IO): Unit = {
      val msg = io.read()
      io.write(msg)
      if (msg == "exit") ()
      else readUntilExit()(io)
    }
  }

  // Declarative version
  object Declarative{
    import MonadicPrograms.{Monad, IO}, IO.Syntax._, Monad.Syntax._

    def readUntilExit[F[_]: IO: Monad](): F[Unit] = ???
  }

}
