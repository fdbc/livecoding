package org.hablapps.fpinscala
package languages

import scala.io.StdIn.readLine
import org.scalatest._

object MonadicPrograms extends MonadicPrograms

class MonadicPrograms extends FlatSpec with Matchers{

  // IO API

  trait IO[P[_]]{
    def read(): P[String]
    def write(msg: String): P[Unit]
  }

  object IO{

    def apply[P[_]](implicit IO: IO[P]) = IO

    object Syntax{
      def read[P[_]]()(implicit IO: IO[P]): P[String] =
        IO.read()

      def write[P[_]](msg: String)(implicit IO: IO[P]): P[Unit] =
        IO.write(msg)

    }

    implicit object ConsoleIO extends IO[Id]{
      def read() = readLine()
      def write(msg: String) = println(msg)
    }
  }

  // Monad API

  trait Monad[P[_]]{
    def returns[A](a: A): P[A]
    def doAndThen[A,B](p: P[A])(f: A => P[B]): P[B]

    // derived

    def map[A, B](ma: P[A])(f: A => B): P[B] =
      doAndThen(ma)(f andThen returns)

    def flatten[A](mma: P[P[A]]): P[A] =
      doAndThen(mma)(identity)

    def iterateUntil[A](ma: P[A])(p: A => Boolean): P[A] =
      doAndThen(ma)(a => if (p(a)) returns(a) else iterateUntil(ma)(p))

  }

  object Monad{
    def apply[P[_]](implicit M: Monad[P]) = M

    object Syntax{
      implicit class DoAndThen[P[_],A](p: P[A])(implicit M: Monad[P]){
        def flatMap[B](f: A => P[B]): P[B] =
          M.doAndThen(p)(f)
        def map[B](f: A => B): P[B] =
          M.map(p)(f)
      }

      def returns[P[_],A](a: A)(implicit M: Monad[P]): P[A] = M.returns(a)
    }
  }

  // API programs

  import IO.Syntax._, Monad.Syntax._

  def echo[P[_]: IO: Monad](): P[String] = for{
    msg <- read()
    _ <- write(msg)
  } yield msg

  // Console-based interpretation of IO Programs

  implicit object MonadId extends Monad[Id]{
    def returns[A](a: A): A = a
    def doAndThen[A,B](p: A)(f: A => B): B = f(p)
  }

  // Echo interpretations

  def consoleEcho(): String =
    echo()(IO.ConsoleIO, Monad[Id])

}