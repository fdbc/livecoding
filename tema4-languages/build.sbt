name := "funcourseinscala"

scalaBinaryVersion := "2.12"

scalaVersion := "2.12.3"

organization := "org.hablapps"

addCompilerPlugin("org.spire-math" %% "kind-projector" % "0.9.3")

libraryDependencies ++= Seq(
  "org.scalaz" %% "scalaz-core" % "7.2.7",
  // "org.scalaz" %% "scalaz-scalacheck-binding" % "7.2.7",
  "org.scalatest" %% "scalatest" % "3.0.1"
)

// initialCommands in console := """
//   | import org.hablapps.fpinscala._
//   | import typeclasses._, diagrams._
//   | import lenguajes._
//   |""".stripMargin
