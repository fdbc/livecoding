package org.hablapps.fpinscala.hofs.homework
package solution

object Homework2 {

  /**
   * Part I.
   *
   * Implement an ADT to represent binary trees with values in internal nodes
   */

  sealed abstract class BinTree[A]
  case class Node[A](left: BinTree[A], a: A, right: BinTree[A]) extends BinTree[A]
  case class Empty[A]() extends BinTree[A]

  /**
   * Part II.
   *
   * Implement these funcitons over binary trees using pattern matching
   */
  object PartII {

    // Sum values of nodes
    def sum(t: BinTree[Int]): Int = t match {
      case Node(left, i, right) => sum(left) + i + sum(right)
      case Empty() => 0
    }

    // return the maximun value
    def max(t: BinTree[Int]): Int = t match {
      case Node(left, i, right) => List(max(left), max(right), i).max
      case Empty() => Int.MinValue
    }

    // Count how many empty trees there are
    def emptyTrees[A](t: BinTree[A]): Int = t match {
      case Node(left, a, right) => emptyTrees(left) + emptyTrees(right)
      case Empty() => 1
    }

    // Calculate the depth of a tree
    def depth[A](t: BinTree[A]): Int = t match {
      case Node(left, a, right) => depth(left) max depth(right) + 1
      case Empty() => 0
    }

  }

  /**
   * Part III.
   *
   * Implement `fold` (catamorphism) for binary trees
   * and modularise the functions implemented with pattern matching in
   * Part II.
   */
  object PartIII {

    def fold[A,Z](t: BinTree[A])(
        leaf: Z,
        node: (Z, A, Z) => Z): Z = t match {
      case Node(left, a, right) => node(fold(left)(leaf, node), a, fold(right)(leaf, node))
      case Empty() => leaf
    }

    def sum(t: BinTree[Int]): Int = fold[Int, Int](t)(0, _ + _ + _)

    def max(t: BinTree[Int]): Int = fold[Int, Int](t)(Int.MinValue, List(_, _, _).max)

    def emptyTrees[A](t: BinTree[A]): Int = fold[A, Int](t)(1, (l, _, r) => l+r)

    def depth[A](t: BinTree[A]): Int = fold[A, Int](t)(0, (l, _, r) => l max r + 1)
  }

}