package org.hablapps.fpinscala.hofs.homework

// Test this exercise with sbt alias `test-hofs-hw2`
object Homework2 {

  /**
   * Part I.
   *
   * Implement an ADT to represent binary trees with values in internal nodes
   */

  sealed abstract class BinTree[A]
  case class Node[A](left: BinTree[A], a: A, right: BinTree[A]) extends BinTree[A]
  case class Empty[A]() extends BinTree[A]

  /**
   * Part II.
   *
   * Implement these funcitons over binary trees using pattern matching
   */
  object PartII {

    // Sum values of nodes
    def sum(t: BinTree[Int]): Int = ???

    // return the maximun value
    def max(t: BinTree[Int]): Int = ???

    // Count how many empty trees there are
    def emptyTrees[A](t: BinTree[A]): Int = ???

    // Calculate the depth of a tree
    def depth[A](t: BinTree[A]): Int = ???

  }

  /**
   * Part III.
   *
   * Implement `fold` (catamorphism) for binary trees
   * and modularise the functions implemented with pattern matching in
   * Part II.
   */
  object PartIII {

    def fold[A,Z](t: BinTree[A])(
        leaf: Z,
        node: (Z, A, Z) => Z): Z = ???

    def sum(t: BinTree[Int]): Int = ???

    def max(t: BinTree[Int]): Int = ???

    def emptyTrees[A](t: BinTree[A]): Int = ???

    def depth[A](t: BinTree[A]): Int = ???
  }

}
