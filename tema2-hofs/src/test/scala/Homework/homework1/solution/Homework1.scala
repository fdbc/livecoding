package org.hablapps.fpinscala.hofs.homework
package solution

object Exercise1 {

  object PartI {
    // Implement `filter` for `Option`
    def filter[A](o: Option[A])(p: A => Boolean): Option[A] = o match {
      case s@Some(a) if p(a) => s
      case _ => None
    }

    // Implement `map` for `Option`
    def map[A, B](o: Option[A])(f: A => B): Option[B] = o match {
      case Some(a) => Some(f(a))
      case None => None
    }
  }

  object PartII {
    // Implement `fold` for `Option`
    def fold[A, B](o: Option[A])(b: B)(f: A => B): B = o match {
      case Some(a) => f(a)
      case None => b
    }

    // Implement `filter` in terms of `fold`
    def filter[A](o: Option[A])(p: A => Boolean): Option[A] =
      fold(o)(Option.empty[A]){ a =>
        if (p(a)) Option(a) else None
      }

    // Implement `map` in terms of `fold`
    def map[A, B](o: Option[A])(f: A => B): Option[B] =
      fold(o)(Option.empty[B])(a => Option(f(a)))
      // fold(o)(Option.empty[B])(f andThen Option.apply) // Using function composition style

  }
}
