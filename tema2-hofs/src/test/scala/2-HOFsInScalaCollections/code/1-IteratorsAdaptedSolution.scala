package org.hablapps.hofs
package forloopsremoval
package withviews

object IteratorsAdaptedSolution extends IteratorsAdaptedSolution
class IteratorsAdaptedSolution extends withloops.ProblemVariation {

  /**
   * Solution: HOFs version
   */

  def lengths(list: List[String]): List[Int] =
    list.iterator.flatMap(_.split(" "))
      .filter(_ != "")
      .map(_.length)
      .toList

  /**
   * Test solution
   */

  describe("Problem variation: iterator version"){
    test(lengths)
  }
}
