package org.hablapps.hofs
package forloopsremoval
package withviews

object ViewsAdaptedSolution extends ViewsAdaptedSolution
class ViewsAdaptedSolution extends withloops.ProblemVariation {

  /**
   * Solution: HOFs version
   */

  def lengths(list: List[String]): List[Int] =
    list.view.flatMap(_.split(" "))
      .filter(_ != "")
      .map(_.length)
      .force // Seq[Int]
      .toList

  /**
   * Test solution
   */

  describe("Problem variation: view version"){
    test(lengths)
  }
}
