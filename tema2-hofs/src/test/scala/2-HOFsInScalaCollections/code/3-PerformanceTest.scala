package org.hablapps.hofs
package forloopsremoval
package withviews

import org.scalameter.api._

class PerformanceTest extends Bench.OfflineReport{

  /** INPUTS */

  val ranges = for {
    size <- Gen.range("size")(30000, 150000, 30000)
  } yield (0 until size).map(_ => "aa").toList

  /** TESTS */

  // remove `def test() = ` to run the test
  def test() = performance of "Lengths" in {

    measure method "Loops" in {
      using(ranges) in withloops.MonolythicAdaptedSolution.lengths
    }

    measure method "Plain HOFs" in {
      using(ranges) in withhofs.HOFsAdaptedSolution.lengths
    }

    measure method "Views" in {
      using(ranges) in ViewsAdaptedSolution.lengths
    }

    measure method "Iterators" in {
      using(ranges) in IteratorsAdaptedSolution.lengths
    }
  }
}


