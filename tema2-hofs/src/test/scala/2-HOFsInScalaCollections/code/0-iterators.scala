package org.hablapps.hofs
package inscalacollections

import org.scalatest._

class Iterators extends FunSpec with Matchers{

  // Iterators can be backed up by collections. Indeed, any Iterable
  // can be traversed through an iterator

  describe("Iterators can be backed up by collections"){

    it("Sets as source of iterators"){

      val set1: Set[Int] = Set(1,2,3)
      val it: Iterator[Int] = set1.iterator

      it.hasNext shouldBe true
      it.next shouldBe 1
      it.next shouldBe 2
      it.next shouldBe 3
      it.hasNext shouldBe false

      an[Exception] shouldBe thrownBy(it.next)
    }

    it("Iterators can be materialized into data structures"){
      val list1: List[Int] = List(1,2,3,4)
      val it: Iterator[Int] = list1.iterator

      it.toSeq shouldBe List(1,2,3,4)
      it.hasNext shouldBe false

      Map(1->"",2->"a",3->"b").iterator.toSeq shouldBe List(1->"",2->"a",3->"b")
    }

  }

  // But iterators can generate data through other means, not necessarily
  // from a data source.

  describe("Iterators created from pure generators"){

    it("An iterator that never ends"){

      val it: Iterator[Int] = Iterator.iterate(0)(_+1)
      it.next shouldBe 0
      it.next shouldBe 1
      it.next shouldBe 2
      // etc.
      // it.toSeq shouldBe List(0,1,2,3,4) never ends
    }

  }


  // Iterators share many operations with collections. However, there is an essential
  // difference between them: in the case of iterators, these operations are "lazy", i.e.
  // they create another iterator, and the results won't be get until this new iterator
  // is materialized or consumed.

  describe("Iterator and laziness"){

    it("A data structure can be mapped in one shot"){
      val l2: List[Int] = List(1,2,3).map{_+1}
      l2 shouldBe List(2,3,4)
    }

    it("When an iterator is mapped, another iterator is created, but nothing has been computed yet"){
      val it2: Iterator[Int] = List(1,2,3).iterator.map(_+1)
      // it2 shouldBe ???
      it2.toList shouldBe List(2,3,4)
    }
  }
}