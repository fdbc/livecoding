package org.hablapps.hofs
package templates
package forloopsremoval
package withviews

object ViewsAdaptedSolution extends ViewsAdaptedSolution
class ViewsAdaptedSolution extends withloops.ProblemVariation {

  /**
   * Solution: HOFs version
   */

  def lengths(list: List[String]): List[Int] =
    ???
    // list.flatMap(_.split(" "))
    //   .filter(_ != "")
    //   .map(_.length)

  /**
   * Test solution
   */

  ignore("6- Problem variation: view version"){
    test(lengths)
  }
}
