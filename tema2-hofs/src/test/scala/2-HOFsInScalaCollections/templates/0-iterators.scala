package org.hablapps.hofs
package templates
package inscalacollections

import org.scalatest._

class Iterators extends FunSpec with Matchers{

  // Iterators can be backed up by collections. Indeed, any Iterable
  // can be traversed through an iterator

  describe("Iterators can be backed up by collections"){

    ignore("Sets as source of iterators"){
      ???
    }

    ignore("Iterators can be materialized into data structures"){
      ???
    }

  }

  // But iterators can generate data through other means, not necessarily
  // from a data source.

  describe("Iterators created from pure generators"){

    ignore("An iterator that never ends"){
      ???
    }

  }


  // Iterators share many operations with collections. However, there is an essential
  // difference between them: in the case of iterators, these operations are "lazy", i.e.
  // they create another iterator, and the results won't be get until this new iterator
  // is materialized or consumed.

  describe("Iterator and laziness"){

    ignore("A data structure can be mapped in one shot"){
      ???
    }

    ignore("When an iterator is mapped, another iterator is created, but nothing has been computed yet"){
      ???
    }
  }
}