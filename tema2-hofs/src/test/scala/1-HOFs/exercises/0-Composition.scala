package org.hablapps.fpinscala.hofs

import org.scalatest._

class CompositionSpec extends FunSpec with Matchers {

  // Implement `compose`

  def compose[A,B,C](g: B => C, f: A => B): A => C = ???

  // Implement `andThen` using `compose`

  def andThen[A,B,C](f: A => B, g: B => C): A => C = ???

  // Tests

  def square(i:Int) = i * i
  def toQuotedString(i:Int) = "'" + i.toString + "'"

  describe("Composition"){
    ignore("should componer dos funciones"){
      compose(toQuotedString, square)(10) shouldBe "'100'"
      andThen(square, toQuotedString)(10) shouldBe "'100'"
    }
  }

}