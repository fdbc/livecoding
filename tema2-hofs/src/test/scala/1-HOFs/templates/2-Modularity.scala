package org.hablapps.fpinscala.hofs
package templates

import org.scalatest._

class HOFModularity extends FunSpec with Matchers {

  // Monolithic functions

  object Monolithic{

    def sum(l: List[Int]): Int =
      l match {
        case Nil => 0
        case x :: r => x + sum(r)
      }

    def concat(l: List[String]): String =
      l match {
        case Nil => ""
        case x :: r => x + concat(r)
      }
  }

  def hofsTest(sum: List[Int] => Int, concat: List[String] => String) = {
    it("should work properly for sums and concatenations"){
      sum(List(1, 2, 3, 4)) shouldBe 10
      concat(List("hola", "a", "todo", "el", "mundo")) shouldBe "holaatodoelmundo"
    }
  }

  ignore("Monolithic functions") {
    import Monolithic._

    hofsTest(sum, concat)
  }

  // Abstractions for modularisations

  object Abstraction{

    def collapse = ???
  }

  // Composition

  object ModularisedFunctions{
    import Abstraction._

    def sum(l: List[Int]): Int = ???

    def concat(l: List[String]): String = ???
  }

  ignore("Modularised functions") {
    import ModularisedFunctions._

    hofsTest(sum, concat)

    import Abstraction._

    it("Collapse can be further reused for other purposes as well"){

    }
  }

}
