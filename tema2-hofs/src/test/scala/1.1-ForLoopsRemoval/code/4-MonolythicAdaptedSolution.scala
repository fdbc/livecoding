package org.hablapps.hofs
package forloopsremoval
package withloops

object MonolythicAdaptedSolution extends MonolythicAdaptedSolution
class MonolythicAdaptedSolution extends ProblemVariation{

  /**
   * Solution: Foreach loops and vars
   */

  def lengths(list: List[String]): List[Int] = {

    var out: List[Int] = List()

    list.foreach{ line =>
      val words: List[String] =
        line.split(" ").toList

      words.foreach{ word =>
        if (word != "") // This is the only variation in the solution
          out = word.length :: out
      }
    }

    out.reverse
  }

  describe("Problem variation: foreach and vars version"){
    test(lengths)
  }
}
