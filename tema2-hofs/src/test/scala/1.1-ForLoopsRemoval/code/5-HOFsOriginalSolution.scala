package org.hablapps.hofs
package forloopsremoval
package withhofs

class HOFsOriginalSolution extends withloops.OriginalProblem {

  /**
   * Solution: HOFs version
   */

  def lengths(list: List[String]): List[Int] =
    list.flatMap(_.split(" "))
      .map(_.length)

  /**
   * Test solution
   */

  describe("Original problem: HOFs version"){
    test(lengths)
  }
}
