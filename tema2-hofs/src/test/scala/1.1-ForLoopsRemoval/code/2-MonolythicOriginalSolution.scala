package org.hablapps.hofs
package forloopsremoval
package withloops

class MonolythicOriginalSolution extends OriginalProblem{

  /**
   * Solution: Foreach loops and vars
   */
  def lengths(list: List[String]): List[Int] = {

    var out: List[Int] = List()

    for (line <- list){
      val words: List[String] =
        line.split(" ").toList
      for (word <- words)
        out = word.length :: out
    }

    out.reverse
  }

  describe("Original problem: foreach and vars version"){
    test(lengths)
  }
}
