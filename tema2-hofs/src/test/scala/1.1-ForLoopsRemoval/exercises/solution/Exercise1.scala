package org.hablapps.hofs
package forloopsremoval
package exercises
package solution

class Exercise1 extends Exercise1Spec{

  /**
   * PART I.
   *
   * Use the `foldLeft` [1] function to implement the `sumEvenNumbers` function.
   *
   * [1] http://www.scala-lang.org/api/2.12.3/scala/collection/immutable/List.html#foldLeft[B](z:B)(op:(B,A)=>B):B
   *
   */
  object WithHOFWithoutFilter{

    def sumEvenNumbers(list: List[Int]): Int =
      list.foldLeft(0)((out, number) =>
        if (number % 2 == 0) number + out
        else out)
  }

  describe("HOF program without filter"){
    test(WithHOFWithoutFilter.sumEvenNumbers)
  }

  /**
   * PART II.
   *
   * Use the `foldLeft` and the `filter` function [1].
   *
   * [1] http://www.scala-lang.org/api/2.12.3/scala/collection/immutable/List.html#filter(p:A=>Boolean):Repr
   */
  object WithHOFWithFilter{

    def sumEvenNumbers(list: List[Int]): Int =
      list.filter(_ % 2 == 0)
        .foldLeft(0)(_ + _)
  }

  describe("HOF program with filter"){
    test(WithHOFWithFilter.sumEvenNumbers)
  }
}