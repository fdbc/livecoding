package org.hablapps.hofs
package forloopsremoval
package exercises

class MonolythicSolution extends Exercise1Spec{

  def sumEvenNumbers(list: List[Int]): Int = {
    var out: Int = 0
    for(number <- list){
      if (number % 2 == 0)
        out += number
    }
    out
  }

  describe("Monolythic program"){
    test(sumEvenNumbers)
  }
}

