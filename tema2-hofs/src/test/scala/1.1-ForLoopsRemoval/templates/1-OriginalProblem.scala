package org.hablapps.hofs
package templates
package forloopsremoval
package withloops

import org.scalatest._

abstract class OriginalProblem extends FunSpec with Matchers{

  /**
   * Implement a `lengths` function that behaves according to the
   * following specification.
   */
  def test(lengths: List[String] => List[Int]){

    it("should calculate lengths for each word"){
      lengths(List("en", "un", "", "lugar", "de", "", "la", "mancha")) shouldBe
        List(2,2,0,5,2,0,2,6)
    }

    it("should split lines into words"){
      lengths(List("en un  lugar", "de  la mancha")) shouldBe
        ???
    }

    it("should map empty lists to empty lists"){
      lengths(List()) shouldBe List()
    }
  }
}