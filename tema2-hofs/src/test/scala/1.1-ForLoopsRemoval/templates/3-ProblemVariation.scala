package org.hablapps.hofs
package templates
package forloopsremoval
package withloops

import org.scalatest._

abstract class ProblemVariation extends FunSpec with Matchers {

  /**
   * In this new version of the problem we just require a small change in the
   * function: empty words should not be taken into account.
   */

  def test(lengths: List[String] => List[Int]){

    it("should calculate lengths for each (non-empty) word"){
      lengths(List("en", "un", "", "lugar", "de", "", "la", "mancha")) shouldBe
        ??? // List(2,2,0,5,2,0,2,6)
    }

    it("should split lines into words"){
      lengths(List("en un  lugar", "de  la mancha")) shouldBe
        List(2,2,5,2,2,6)
    }

    it("should map empty lists to empty lists"){
      lengths(List()) shouldBe
        List()
    }

    // Additional tests

    it("should map lists full of empty words to the empty list"){
      lengths(List("","","","","")) shouldBe
        ???
    }

    it("should map lists full of space-only words to the empty list"){
      lengths(List("      ", "          ")) shouldBe
        ???
    }
  }
}
