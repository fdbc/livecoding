package org.hablapps.fpinscala.hofs
package diagrams
package code

import org.scalatest._

class DrawingReprSpec extends FunSpec with Matchers{
  import PictureDSL._, DrawingRepr._
  import Picture.Syntax._
  
  describe("Drawings of pictures"){

    it("should work for pictures of a single rectangle"){
      Drawing(Rectangle(1,1).picture) shouldBe 
        List((Pos(0,0),List(),Rectangle(1,1)))
    }

    it("should work for pictures of a single triangle"){
      Drawing(Triangle(1).picture) shouldBe 
        List((Pos(0,0),List(),Triangle(1)))
    }

    it("should work for pictures of a single circle"){
      Drawing(Circle(1).picture) shouldBe
        List((Pos(0,0),List(),Circle(1)))
    }

    it("should work for simple 'above' compositions"){
      Drawing(Triangle(1).picture above Rectangle(0.5,2).picture) shouldBe
        List((Pos(0,1),List(),Triangle(1)),
          (Pos(0,-Triangle(1).height/2),List(),Rectangle(0.5,2)))
    }

    it("should work for simple 'beside' compositions"){
      Drawing(Triangle(1).picture beside Rectangle(2,0.5).picture) shouldBe 
        List((Pos(-1,0),List(),Triangle(1)),
          (Pos(0.5,0),List(),Rectangle(2,0.5)))
    }

    it("should work for complex compositions"){
      Drawing(Triangle(1).picture beside Rectangle(2,0.5).picture
        above Circle(1).picture) shouldBe List(
        (Pos(-1,1),List(),Triangle(1)),
        (Pos(0.5,1),List(),Rectangle(2,0.5)),
        (Pos(0,-Triangle(1).height/2),List(),Circle(1)))
    }
  }

  describe("Extents of pictures"){
  
    it("should work for pictures of a single rectangle"){
      Drawing.extent(Drawing(Rectangle(1,1).picture)) shouldBe 
        ((Pos(-0.5,-0.5), Pos(0.5,0.5)))
    }

    it("should work for pictures of a single triangle"){
      Drawing.extent(Drawing(Triangle(1).picture)) shouldBe 
        ((Pos(-0.5,-Triangle(1).height/2), Pos(0.5,Triangle(1).height/2)))
    }

    it("should work for pictures of a single circle"){
      Drawing.extent(Drawing(Circle(1).picture)) shouldBe
        ((Pos(-1,-1), Pos(1,1)))
    }

    it("should work for simple 'above' compositions"){
      Drawing.extent(Drawing(
        Triangle(1).picture above Rectangle(0.5,2).picture)) shouldBe
        ((Pos(-0.5,-(2+Triangle(1).height)/2), Pos(0.5,(2+Triangle(1).height)/2)))
    }

    it("should work for simple 'beside' compositions"){
      Drawing.extent(Drawing(Triangle(1).picture beside Rectangle(2,0.5).picture)) shouldBe 
        ((Pos(-1.5,-Triangle(1).height/2), Pos(1.5,Triangle(1).height/2)))
    }

    it("should work for complex compositions"){
      Drawing.extent(Drawing(Triangle(1).picture beside Rectangle(2,0.5).picture
        above Circle(1).picture)) shouldBe 
        ((Pos(-1.5,-(Triangle(1).height + 2)/2), Pos(1.5,(Triangle(1).height + 2)/2)))
    }
  }
}