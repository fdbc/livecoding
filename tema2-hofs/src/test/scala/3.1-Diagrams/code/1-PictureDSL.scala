package org.hablapps.fpinscala.hofs
package diagrams
package code

object PictureDSL{

  /**
   * SHAPE ADT
   */

  // Shape = Rectangle + Circle + Triangle
  //       = Double * Double + Double + Double

  sealed abstract class Shape
  case class Rectangle(width: Double, height: Double) extends Shape
  case class Circle(radius: Double) extends Shape
  case class Triangle(width: Double) extends Shape

  object Shape{

    def fold[B](s: Shape)(
        rectangle: (Double, Double) => B,
        circle: Double => B,
        triangle: Double => B): B =
      s match {
        case Rectangle(width, height) => rectangle(width, height)
        case Circle(radius) => circle(radius)
        case Triangle(width) => triangle(width)
      }
  }

  /**
   * STYLE SHEETS
   */

  type StyleSheet = List[Styling]

  // Styling = FillColor + StrokeColor + StrokeWidth
  //         = Color + Color + Double

  sealed abstract class Styling
  case class FillColor(c: Color) extends Styling
  case class StrokeColor(c: Color) extends Styling
  case class StrokeWidth(w: Double) extends Styling

  object Styling{

    def fold[T](s: Styling)(
        fillColor: Color => T,
        strokeColor: Color => T,
        strokeWidth: Double => T) = s match {
      case FillColor(c) => fillColor(c)
      case StrokeColor(c) => strokeColor(c)
      case StrokeWidth(w) => strokeWidth(w)
    }
  }

  // Color = Red + Blue + Green + ....
  //       = 1 + 1 + 1 + ....

  sealed abstract class Color
  case object Red extends Color
  case object Blue extends Color
  case object Green extends Color
  case object Yellow extends Color
  case object Brown extends Color
  case object Black extends Color
  case object Bisque extends Color
  case object Maroon extends Color
  case object LightGray extends Color
  case object Purple extends Color
  case object NavyBlue extends Color
  case object DarkGreen extends Color
  case object Teal extends Color
  case object Alpha extends Color

  /**
   * Pictures
   */

  // Picture = Place + Above + Beside
  //         = StyleSheet * Shape + Picture * Picture + Picture * Picture

  sealed abstract class Picture
  case class Place(style: StyleSheet, shape: Shape) extends Picture
  case class Above(top: Picture, bottom: Picture) extends Picture
  case class Beside(left: Picture, right: Picture) extends Picture

  object Picture{

    def fold[T](p: Picture)(
      place: (StyleSheet, Shape) => T,
      above: (T, T) => T,
      beside: (T, T) => T): T =
      p match {
        case Place(st,sh) => place(st,sh)
        case Above(t,b) => above(fold(t)(place,above,beside), fold(b)(place, above, beside))
        case Beside(l,r) => beside(fold(l)(place, above, beside), fold(r)(place, above, beside))
      }

    // Syntax

    object Syntax{

      implicit class TriangleOps(t: Triangle){
        def height(): Double =
          t.width * math.sqrt(3) / 2
      }

      implicit class ShapeOps(sh: Shape){
        def picture() =
          Place(List(),sh)
        def picture(ss: Styling*) =
          Place(ss.toList,sh)
      }

      implicit class PictureOps(p: Picture) {
        def above(other: Picture): Picture =
          Above(p, other)
        def beside(other: Picture): Picture =
          Beside(p, other)
        def *(other: Picture): Picture = beside(other)
      }
    }
  }
}
