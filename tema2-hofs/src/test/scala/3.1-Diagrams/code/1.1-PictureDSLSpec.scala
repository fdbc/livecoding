package org.hablapps.fpinscala.hofs
package diagrams
package code

import org.scalatest._

class PictureDSLSpec extends FunSpec with Matchers{
  import PictureDSL._, Picture.Syntax._

  describe("Syntax of pictures"){
  
    it("should work for pictures of a single rectangle"){
      Rectangle(1,1).picture shouldBe 
        Place(List(),Rectangle(1,1))
    }

    it("should work for pictures of a single triangle"){
      Triangle(1).picture shouldBe 
        Place(List(),Triangle(1))
    }

    it("should work for pictures of a single circle"){
      Circle(1).picture shouldBe
        Place(List(),Circle(1))
    }

    it("should work for simple 'above' compositions"){
      Triangle(1).picture above Rectangle(0.5,2).picture shouldBe
        Above(Place(List(),Triangle(1)),
          Place(List(),Rectangle(0.5,2)))
    }

    it("should work for simple 'beside' compositions"){
      Triangle(1).picture beside Rectangle(2,0.5).picture shouldBe 
        Beside(Place(List(),Triangle(1)),
          Place(List(),Rectangle(2,0.5)))
    }

    it("should work for complex compositions"){
      Triangle(1).picture beside Rectangle(2,0.5).picture above Circle(1).picture shouldBe 
        Above(Beside(Place(List(),Triangle(1)),
          Place(List(),Rectangle(2,0.5))),
          Place(List(),Circle(1)))
    }
  }
}