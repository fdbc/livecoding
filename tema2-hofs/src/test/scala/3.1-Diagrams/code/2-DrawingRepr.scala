package org.hablapps.fpinscala.hofs
package diagrams
package code

object DrawingRepr{
  import PictureDSL._

  // Auxiliary type

  case class Pos(x: Double, y: Double) {
    def +(other: Pos): Pos = Pos(x+other.x, y+other.y)
  }

  /**
   * Drawings
   */

  type Drawing = List[Drawing.Component]

  object Drawing{

    type Component = (Pos, StyleSheet, Shape)

    // Drawing constructor: picture interpreter

    def apply(p: Picture): Drawing =
      Picture.fold(p)(drawShape, drawAbove, drawBeside)

    def drawShape(ss: StyleSheet, s: Shape): Drawing =
      (Pos(0,0), ss, s) :: Nil

    def drawAbove(d: Drawing, bottom: Drawing): Drawing = {
      val (Pos(_, toplly), _) = extent(d)
      val (_, Pos(_, bottomury)) = extent(bottom)

      transformDrawing(Pos(0, bottomury))(d) :::
      transformDrawing(Pos(0, toplly))(bottom)
    }

    def drawBeside(d: Drawing, right: Drawing): Drawing = {
      val (_, Pos(lefturx, _)) = extent(d)
      val (Pos(rightllx, _), _) = extent(right)

      transformDrawing(Pos(rightllx, 0))(d) :::
      transformDrawing(Pos(lefturx, 0))(right)
    }

    def transformDrawing(t: Pos): Drawing => Drawing =
      _ map {
        case (t2, ss, s) => (t + t2, ss, s)
      }

    // Lower left and upper right corners

    type Extent = (Pos, Pos)

    def extent(drawing: Drawing): Extent =
      drawing.foldLeft((Pos(Double.MaxValue, Double.MaxValue), Pos(Double.MinValue, Double.MinValue))) {
        (acc, x) => union(acc, extent(x))
      }

    def extent(t: Component): Extent = {
        val (ll, ur) = extent(t._3)
        (t._1 + ll, t._1 + ur)
      }

    def extent(s: Shape): Extent = s match {
      case Rectangle(width, height) =>
        (Pos(-width/2, -height/2), Pos(width/2, height/2))
      case Circle(radius) =>
        (Pos(-radius, -radius), Pos(radius, radius))
      case Triangle(width) =>
        (Pos(-width/2, -math.sqrt(3)*(width/4)), Pos(width/2, math.sqrt(3)*(width/4)))
    }

    def union(e1: Extent, e2: Extent): Extent =
      (
        Pos(
          math.min(e1._1.x, e2._1.x),
          math.min(e1._1.y, e2._1.y)),
        Pos(
          math.max(e1._2.x, e2._2.x),
          math.max(e1._2.y, e2._2.y))
      )
  }
}