package org.hablapps.fpinscala.hofs
package diagrams
package templates

import org.scalatest._

class SVGSpec extends FunSpec with Matchers{
  import PictureDSL._, Picture.Syntax._
  
  describe("SVG of pictures"){
    ignore("should work for complex compositions"){
  //     SVG(Triangle(100).picture(FillColor(Bisque)) beside Rectangle(200,50).picture(FillColor(Red))
  //       above Circle(100).picture(FillColor(Blue))) shouldBe 
  // """|<svg width="300.0" height="286.60254037844385" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
  //    |  <g transform="translate(150.0, 143.30127018922192)">
  //    |    <g transform="scale(1, -1)">
  //    |      <polygon points="-150.0,56.69872981077807 -100.0,143.30127018922192 -50.0,56.69872981077807" fill=#000 />
  //    |      <rect x="-50.0" y="75.0" width="200.0" height="50.0" fill=#f00 />
  //    |      <circle r="100.0" cx="0.0" cy="-43.30127018922193" fill=#00f />
  //    |    </g>
  //    |  </g>
  //    |</svg>""".stripMargin
    }
  }
}