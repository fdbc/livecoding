package org.hablapps.fpinscala.hofs
package diagrams
package templates

import org.scalatest._

class DrawingReprSpec extends FunSpec with Matchers{
  import PictureDSL._, DrawingRepr._
  import Picture.Syntax._
  
  ignore("Drawings of pictures"){

    it("should work for pictures of a single rectangle"){
      // Drawing(Rectangle(1,1).picture) shouldBe (??? : Drawing)
    }

    it("should work for pictures of a single triangle"){
      // Drawing(Triangle(1).picture) shouldBe (??? : Drawing)
    }

    it("should work for pictures of a single circle"){
      // Drawing(Circle(1).picture) shouldBe (??? : Drawing)
    }

    it("should work for simple 'above' compositions"){
      // Drawing(Triangle(1).picture above Rectangle(0.5,2).picture) shouldBe (??? : Drawing)
    }

    it("should work for simple 'beside' compositions"){
      // Drawing(Triangle(1).picture beside Rectangle(2,0.5).picture) shouldBe (??? : Drawing)
    }

    it("should work for complex compositions"){
      // Drawing(Triangle(1).picture beside Rectangle(2,0.5).picture
      //   above Circle(1).picture) shouldBe (??? : Drawing)
    }
  }

  ignore("Extents of pictures"){
  
    it("should work for pictures of a single rectangle"){
      // Drawing.extent(Drawing(Rectangle(1,1).picture)) shouldBe (??? : Extent)
    }

    it("should work for pictures of a single triangle"){
      // Drawing.extent(Drawing(Triangle(1).picture)) shouldBe (??? : Extent)
    }

    it("should work for pictures of a single circle"){
      // Drawing.extent(Drawing(Circle(1).picture)) shouldBe (??? : Extent)
    }

    it("should work for simple 'above' compositions"){
      // Drawing.extent(Drawing(
      //   Triangle(1).picture above Rectangle(0.5,2).picture)) shouldBe (??? : Extent)
    }

    it("should work for simple 'beside' compositions"){
      // Drawing.extent(Drawing(Triangle(1).picture beside Rectangle(2,0.5).picture)) shouldBe  (??? : Extent)
    }

    it("should work for complex compositions"){
      // Drawing.extent(Drawing(Triangle(1).picture beside Rectangle(2,0.5).picture
      //   above Circle(1).picture)) shouldBe  (??? : Extent)
    }
  }
}