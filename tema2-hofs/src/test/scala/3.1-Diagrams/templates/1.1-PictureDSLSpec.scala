package org.hablapps.fpinscala.hofs
package diagrams
package templates

import org.scalatest._

class PictureDSLSpec extends FunSpec with Matchers{
  import PictureDSL._, Picture.Syntax._

  ignore("Syntax of pictures"){
  
    it("should work for pictures of a single rectangle"){
      
    }

    it("should work for pictures of a single triangle"){
      
    }

    it("should work for pictures of a single circle"){
      
    }

    it("should work for simple 'above' compositions"){
      
    }

    it("should work for simple 'beside' compositions"){
      
    }

    it("should work for complex compositions"){
      
    }
  }
}