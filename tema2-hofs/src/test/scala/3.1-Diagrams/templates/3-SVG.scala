package org.hablapps.fpinscala.hofs
package diagrams
package templates

object SVG {
  import PictureDSL._, DrawingRepr._

  // Picture interpreter

  def apply(picture: Picture): String =
    ???
  
  // Drawing interpreter

  def apply(drawing: Drawing): String = {
    // HEADER
    val (width, height) = size(drawing)
    val header =
      s"""|<svg width="$width" height="$height" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
          |  <g transform="translate(${width/2}, ${height/2})">
          |    <g transform="scale(1, -1)">""".stripMargin + "\n      "

    // SHAPES
    val draw = ???

    // FOOTER
    val footer =
      s"""|    </g>
          |  </g>
          |</svg>""".stripMargin

    // ALL TOGETHER
    header ++ draw ++ footer
  }

  // Size of drawing

  def size(d: Drawing): (Double, Double) = ???

  // Style interpreter

  def styleToSVG(style: StyleSheet): String = ???

  // Shape interpreter

  def shapeToSVG(s: Shape): Pos => StyleSheet => String = ???
}