// package org.hablapps.fpinscala.hofs
// package solution

// // Exercise: Implement tetris figures using the Diagrams library

// // you can test this exercise using the alias `test-hofs-tetris`

// object Tetris extends App {

//   object Tetris {
//     import diagrams.code._, Picture._

//     // This picture represents a 1x1 square of given color. Use it
//     // as a building block to create other figures.
//     def block(c: Color): Picture =
//       Place(FillColor(c) :: Nil, Rectangle(100, 100))

//     val I: Picture =
//       block(Maroon) above
//       block(Maroon) above
//       block(Maroon) above
//       block(Maroon)

//     val J: Picture =
//       (block(Alpha)     beside block(LightGray)) above
//       (block(Alpha)     beside block(LightGray)) above
//       (block(LightGray) beside block(LightGray))

//     // Use `Purple` color for this figure
//     val L: Picture =
//       (block(Purple) beside block(Alpha)) above
//       (block(Purple) beside block(Alpha)) above
//       (block(Purple) beside block(Purple))

//     // Use `NavyBlue` color for this figure

//     val O: Picture =
//       (block(NavyBlue) beside block(NavyBlue)) above
//       (block(NavyBlue) beside block(NavyBlue))

//     val S: Picture =
//       (block(Alpha)     beside block(DarkGreen) beside block(DarkGreen)) above
//       (block(DarkGreen) beside block(DarkGreen) beside block(Alpha))

//     val T: Picture =
//       (block(Brown) beside block(Brown) beside block(Brown)) above
//       (block(Alpha) beside block(Brown) beside block(Alpha))

//     val Z: Picture =
//       (block(Teal) beside block(Teal) beside block(Alpha)) above
//       (block(Alpha) beside block(Teal) beside block(Teal))

//     def show() = {
//       println("L:")
//       println(SVG.toSVG(Drawing(L)))
//       println("O:")
//       println(SVG.toSVG(Drawing(O)))
//       println("S:")
//       println(SVG.toSVG(Drawing(S)))
//       println("T:")
//       println(SVG.toSVG(Drawing(T)))
//       println("Z:")
//       println(SVG.toSVG(Drawing(Z)))
//     }
//   }

// }
