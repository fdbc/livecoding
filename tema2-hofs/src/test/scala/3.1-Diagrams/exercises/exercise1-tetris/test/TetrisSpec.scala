// package org.hablapps.fpinscala.hofs
// package test

// import org.scalatest._
// import diagrams.code._

// // Test this exercise using alias `test-hofs-tetris`
// class TetrisSpec extends FlatSpec with Matchers {

//   "Tetris" should "generate correct shape for L" in {
//     Tetris.L shouldBe
//       Above(
//         Above(
//           Beside(Tetris.block(Purple), Tetris.block(Alpha)),
//           Beside(Tetris.block(Purple), Tetris.block(Alpha))),
//         Beside(Tetris.block(Purple), Tetris.block(Purple)))
//   }

//   it should "generate correct shape for O" in {
//     Tetris.O shouldBe
//       Above(
//         Beside(Tetris.block(NavyBlue), Tetris.block(NavyBlue)),
//         Beside(Tetris.block(NavyBlue), Tetris.block(NavyBlue)))
//   }

//   it should "generate correct shape for S" in {
//     Tetris.S shouldBe
//       Above(
//         Beside(
//           Beside(Tetris.block(Alpha), Tetris.block(DarkGreen)),
//           Tetris.block(DarkGreen)),
//         Beside(
//           Beside(Tetris.block(DarkGreen), Tetris.block(DarkGreen)),
//           Tetris.block(Alpha)))
//   }

//   it should "generate correct shape for T" in {
//     Tetris.T shouldBe
//       Above(
//         Beside(
//           Beside(Tetris.block(Brown), Tetris.block(Brown)),
//           Tetris.block(Brown)),
//         Beside(
//           Beside(Tetris.block(Alpha), Tetris.block(Brown)),
//           Tetris.block(Alpha)))
//   }

//   it should "generate correct shape for Z" in {
//     Tetris.Z shouldBe
//       Above(
//         Beside(
//           Beside(Tetris.block(Teal), Tetris.block(Teal)),
//           Tetris.block(Alpha)),
//         Beside(
//           Beside(Tetris.block(Alpha), Tetris.block(Teal)),
//           Tetris.block(Teal)))
//   }

// }
