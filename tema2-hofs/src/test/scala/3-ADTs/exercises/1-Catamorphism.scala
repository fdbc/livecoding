package org.hablapps.fpinscala.hofs

import org.scalatest._

// Exercise: Given the catamorphism 'fold' for lists,
// use it to implement `filter`.

// Test this exercise using the sbt alias `test-hofs-fold`
class CatamorphismSpec extends FunSpec with Matchers {

  // This is the `List` definition found in the slides
  sealed abstract class List[A]
  object List {
    def apply[A](as: A*):List[A] = {
      if (as.isEmpty) Nil()
      else Cons(as.head, apply(as.tail: _*))
    }
  }
  case class Cons[A](head: A, tail: List[A]) extends List[A]
  case class Nil[A]() extends List[A]

  // This is the `fold` definition for Lists that your can find in the slides
  def fold[A,B](l: List[A])(nil: B, cons: (A,B) => B): B = l match {
    case Nil() => nil
    case Cons(h, t) => cons(h, fold(t)(nil, cons))
  }

  // Implement the filter function using fold

  def filter[A](l: List[A])(p: A => Boolean): List[A] = ???

  // Tests

  ignore("FilterAsFold"){
    it("should work for integer lists"){
      val l1 = List(1, 2, 3)
      val l2 = List(1,2,3,4,5)
      filter(l1)(_ % 2 != 0) shouldBe List(1, 3)
      filter(l2)(_ % 2 != 0) shouldBe List(1, 3, 5)
    }

    it("should work for string lists"){
      val l1 = List("","hola","que","","tal")
      filter(l1)(!_.isEmpty) shouldBe List("hola", "que", "tal")
    }
  }

}