package org.hablapps.fpinscala.hofs
package templates

object ADTs {

  // 3.1. Multiplying types.

  object Products {

    // Int x Boolean


    // Unit x Int


    // Person = String x Int
  }

  // 3.2. Adding types (coproducts)

  object Sums {

    // Int + Boolean


    // Unit + Int


    // Char + Nothing


    // Vehicle = Bike + Car


    // StringList = Unit + String x StringList

    // MaybeString = Unit + String

  }

  object PatternMaching{
    import Sums._

    // Patter matching

  }

  object Generic{
    // (I) Monolythic programs (data types)

    // (II) Abstraction

    // (III) Modularised programs
    type StringListMod = Nothing
    type BooleanListMod = Nothing
  }

  object StandardADTs {

    // 3.1. Multiplying types.
    object StandardProducts {
      // Int x Boolean

      // Unit x Int

      // String x Int
    }

    // 3.2. Adding types (coproducts)
    object StandardSums {
      // Int + Boolean

      // Unit + Int

      // Nothing + Boolean
    }

    // Which is the algebraic structure of `Option`al values?

    // Implementing lists in Scala - algebraically
  }
}