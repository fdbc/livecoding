package org.hablapps.fpinscala.hofs

object ADTs {

  // 3.1. Multiplying types.

  object Products {

    // Int x Boolean

    case class IntAndBoolean(anInt: Int, aBoolean: Boolean)
    val t1: IntAndBoolean = IntAndBoolean(3, true)

    // Unit x Int

    case class UnitAndInt(aUnit: Unit, anInt: Int)
    val t2: UnitAndInt = UnitAndInt((), 4)

    // Person = String x Int

    case class Person(name: String, age: Int)
    val t3: Person = Person("John Doe", 26)
  }

  // 3.2. Adding types (coproducts)

  object Sums {

    // Int + Boolean

    sealed abstract class IntOrBoolean
    case class AnInt(value: Int) extends IntOrBoolean
    case class ABoolean(value: Boolean) extends IntOrBoolean
    val t1: IntOrBoolean = AnInt(4)

    // Unit + Int

    sealed abstract class UnitOrString
    case class AUnit(value: Unit) extends UnitOrString
    case class AString(value: String) extends UnitOrString

    val t2: UnitOrString = AString("")

    // Char + Nothing

    sealed abstract class CharOrNothing
    case class AChar(value: Char) extends CharOrNothing
    case class ANothing(value: Nothing) extends CharOrNothing

    val t3: CharOrNothing = AChar('a')

    // Vehicle = Bike + Car

    sealed abstract class Vehicle
    case class Bike(owner: String) extends Vehicle
    case class Car(seats: Int) extends Vehicle

    // StringList = Unit + String x StringList

    sealed trait StringList
    case class EmptyStringList() extends StringList
    case class NonEmptyStringList(
      elemento: String,
      resto: StringList) extends StringList

    // MaybeString = Unit + String

    sealed abstract class MaybeString
    case class NoString() extends MaybeString
    case class SomeString(a: String) extends MaybeString
  }

  object PatternMaching{
    import Sums._

    // Patter matching
    def duplicateString(l: StringList): StringList =
      l match {
        case EmptyStringList() =>
          EmptyStringList()
        case NonEmptyStringList(head, tail) =>
          NonEmptyStringList(head, NonEmptyStringList(head, duplicateString(tail)))
      }

    def head(list: StringList): MaybeString =
      list match {
        case EmptyStringList() =>
          NoString()
        case NonEmptyStringList(head, tail) =>
          SomeString(head)
      }

    def tail(list: StringList): MaybeString =
      list match {
        case EmptyStringList() =>
          NoString()
        case NonEmptyStringList(head, EmptyStringList()) =>
          SomeString(head)
        case NonEmptyStringList(head, tailList) =>
          tail(tailList)
      }
  }

  // 5.1. It can be applied to data types
  object Estructuras {
    // (I) Monolythic programs (data types)
    sealed trait StringList
    case class NilString() extends StringList
    case class ConsString(
      elemento: String,
      resto: StringList) extends StringList

    sealed trait BooleanList
    case class NilBoolean() extends BooleanList
    case class ConsBoolean(
      elemento: Boolean,
      resto: BooleanList) extends BooleanList

    // (II) Abstraction
    sealed trait List[T]
    case class Nil[T]()    extends List[T]
    case class Cons[T](
      elemento: T     ,
      resto: List[T]   ) extends List[T]

    // (III) Modularised programs
    type StringListMod = List[String]
    type BooleanListMod = List[Boolean]
  }

  object StandardADTs {

    // 3.1. Multiplying types.
    object StandardProducts {
      // Int x Boolean
      type T1 = Tuple2[Int, Boolean]
      val t1: T1 = (3, true)

      // Unit x Int
      type T2 = (Unit, Int)
      val t2: T2 = ((), 4)

      // String x Int
      case class T3(name: String, age: Int)
      val t3: T3 = T3("John Doe", 26)
    }

    // 3.2. Adding types (coproducts)
    object StandardSums {
      // Int + Boolean
      type T1 = Either[Int, Boolean]
      val t1: T1 = Left(4)

      // Unit + Int
      type T2 = Either[Unit, Int]
      val t2: T2 = Right(18)

      // Nothing + Boolean
      type T3 = Either[Nothing, Boolean]
      val t3: T3 = Right(true)
    }

    // 3.3. Defining ADTs in Scala from scratch.
    object CasoGeneral {

      // Which is the algebraic structure of `Option`al values?
      sealed abstract class Option[A]
      case class Some[A](a: A) extends Option[A]
      case class None[A]() extends Option[A]

      // Implementing lists in Scala - algebraically
      sealed abstract class List[A]
      case class Cons[A](h: A, t: List[A]) extends List[A]
      case class Nil[A]() extends List[A]
    }
  }
}