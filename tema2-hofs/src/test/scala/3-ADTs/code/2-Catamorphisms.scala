package org.hablapps.fpinscala.hofs.hofs

import org.scalatest._

// 8. Catamorphisms are usually higher-order functions that allow us
//    to interpret any ADT in a compositional way (taking into account
//    its structure and the meaning or interpretations of their parts).

class Catamorphism extends FunSpec with Matchers{

  // Catamorphism for `Either`

  def fold[A,B,C](e: Either[A,B])(left: A => C, right: B => C): C =
    e match {
      case Left(a) => left(a)
      case Right(b) => right(b)
    }

  describe("fold for Either"){
    it("should work"){
      fold(Left[String,Int](""))(_.length, identity) shouldBe 0
      fold(Right[String,Int](3))(_.length, identity) shouldBe 3
    }
  }

  // Catamorphism for `List`

  def fold[A, B](l: List[A])
      (nil: B, cons: (A, B) => B): B =
    l match {
      case h :: t => cons(h, fold(t)(nil, cons))
      case Nil => nil
    }

  describe("fold for List"){
    it("should work"){
      fold(Nil: List[String])(0,(s: String, i: Int) => s.length + i) shouldBe 0
      fold(List("","a","b"))(0,(s: String, i: Int) => s.length + i) shouldBe 2
    }
  }

  // Lots of functions can be defined in terms of catamorphisms,
  // e.g. adding integers

  def sum(l: List[Int]): Int = fold[Int, Int](l)(0, _ + _)

}

